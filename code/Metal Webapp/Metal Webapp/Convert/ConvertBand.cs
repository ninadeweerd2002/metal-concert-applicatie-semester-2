﻿using DAL;
using BLL;
using Metal_Webapp.Models;
namespace Metal_Webapp 
{ 
    class ConvertBand
    {
        public BandViewModel ConvertToBandViewModel(BandModel c)
        {
            BandViewModel band = new BandViewModel
            {

                BandID = c.BandID,
                BandNaam = c.BandNaam,
                BandInfo = c.BandInfo,

            };
            return band;
        }
        public BandModel ConvertBandModel(BandViewModel c)
        {
            BandModel band = new BandModel(c.BandID, c.BandNaam, c.BandInfo);
            return band;
        }
    }
}
