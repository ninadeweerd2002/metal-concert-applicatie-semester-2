﻿using DAL;
using BLL;
using Metal_Webapp.Models;

namespace Metal_Webapp
{
    class ConvertConcertzaal
    {
        public ConcertzaalViewModel ConvertToConcertzaalViewModel(ConcertzaalModel c)
        {
            ConcertzaalViewModel concertzaal = new ConcertzaalViewModel
            {

                ConcertzaalID = c.ConcertzaalID,
                ConcertzaalNaam = c.ConcertzaalNaam,
                Locatie = c.Locatie,
                Land = c.Land,

            };
            return concertzaal;
        }

        public ConcertzaalModel ConvertConcertzaalModel(ConcertzaalViewModel c)
        {
            ConcertzaalModel concertzaal = new ConcertzaalModel(c.ConcertzaalID, c.ConcertzaalNaam, c.Locatie, c.Land);
            return concertzaal;
        }
    }
}
