﻿using DAL;
using BLL;
using Metal_Webapp.Models;
namespace Metal_Webapp
{
    class ConvertGebruiker
    {
        public GebruikerViewModel ConvertToGebruikerViewModel(GebruikerModel c)
        {
            GebruikerViewModel gebruiker = new GebruikerViewModel
            {

                ID = c.ID,
                Gebruikersnaam = c.Gebruikersnaam,
                Email = c.Email,
                Password = c.Password,
                BandID = c.BandID,
                BandNaam = c.BandNaam,
                GebruikerID = c.GebruikerID,
            };
            return gebruiker;
        }

        public GebruikerModel ConvertGebruikerModel(GebruikerViewModel c)
        {
            GebruikerModel concertzaal = new GebruikerModel(c.ID, c.GebruikerID, c.Gebruikersnaam, c.Email, c.Password, c.BandID, c.BandNaam);
            return concertzaal;
        }
    }
}
