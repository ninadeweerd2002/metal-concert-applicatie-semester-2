﻿using DAL;
using BLL;
using Metal_Webapp.Models;

namespace Metal_Webapp
{
    class ConvertConcert
    {
        public ConcertViewModel ConvertToConcertViewModel(ConcertModel c)
        {
            ConcertViewModel concert = new ConcertViewModel
            {
                ID = c.ID,
                BandID = c.BandID,
                BandNaam = c.BandNaam,
                BandInfo = c.BandInfo,
                ConcertzaalID = c.ConcertzaalID,
                ConcertzaalNaam = c.ConcertzaalNaam,
                Locatie = c.Locatie,
                Land = c.Land,
                Datum = c.Datum,
                BeginTijd = c.BeginTijd,
                EindTijd = c.EindTijd,
        };
            return concert;
        }

        public ConcertModel ConvertConcertModel(ConcertViewModel c)
        {
            ConcertModel concert = new ConcertModel(c.ID, c.BandID, c.BandNaam, c.BandInfo, c.ConcertzaalID, c.ConcertzaalNaam, c.Locatie, c.Land, c.Datum, c.BeginTijd, c.EindTijd);
            return concert;
        }

    }
}
