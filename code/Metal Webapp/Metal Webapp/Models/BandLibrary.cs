﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace Metal_Webapp.Models
{
    public class BandLibrary
    {
        public int BandModelID { get; set; }
        public string Gebruikersnaam { get; set; }
        public string Email { get; set; }
        public string[] Bandjes { get; set; }
        public string Password { get; set; }
        public List<BandViewModel> BandList { get; set; }
        public IEnumerable<BandViewModel> BandListIE { get; set; }
        public IEnumerable<SelectListItem> SelectListBand { get; set; }

        public BandViewModel Band { get; set; }
        public ConcertViewModel Concert { get; set; }
        public ConcertzaalViewModel ConcertzaalInfo { get; set; }
        public List<ConcertzaalViewModel> ConcertzaalList { get; set; }
        public ConcertViewModel ConcertInfo { get; set; }
        public List<ConcertViewModel> ConcertList { get; set; }
        public GebruikerViewModel Gebruiker { get; set; }
        public List<GebruikerViewModel> GebruikerList { get; set; }

    }
}
