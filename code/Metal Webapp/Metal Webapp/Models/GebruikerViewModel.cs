﻿using BLL;
using DAL;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace Metal_Webapp.Models
{
    public class GebruikerViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = " Vul dit veld in!", AllowEmptyStrings = false)]
        public string Gebruikersnaam { get; set; }

        [Required(ErrorMessage = " Vul dit veld in!", AllowEmptyStrings = false)]
        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Ongeldige Email Adres")]
        public string Email { get; set; }

        [Required(ErrorMessage = " Vul dit veld in!", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public int BandID { get; set; }
        public string BandNaam { get; set; }
        public int GebruikerID { get; set; }

        public GebruikerViewModel(int id, int gebruikerid, string gb, string email, string pw, int fanBandID)
        {
            ID = id;
            Gebruikersnaam = gb;
            Email = email;
            Password = pw;
            BandID = fanBandID;
            GebruikerID = gebruikerid;
        }
        public GebruikerViewModel(string gb, string pw)
        {
            Gebruikersnaam = gb;
            Password = pw;

        }

        public GebruikerViewModel()
        {
        }
        public GebruikerViewModel(GebruikerModel c)
        {
            ID = c.ID;
            Gebruikersnaam = c.Gebruikersnaam;
            Email = c.Email;
            Password = c.Password;
            BandID = c.BandID;
            BandNaam = c.BandNaam;
            GebruikerID = c.GebruikerID;
        }
    }
}
