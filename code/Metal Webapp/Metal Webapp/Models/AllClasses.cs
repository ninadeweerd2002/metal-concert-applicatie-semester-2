﻿using Microsoft.AspNetCore.Mvc.Rendering;
namespace Metal_Webapp.Models
{
    public class AllClasses
    {
        public int BandModelID { get; set; }      
        public int ConcertzaalModelID { get; set; }
        public DateTime Datum { get; set; }
        public string BeginTijd { get; set; }
        public string EindTijd { get; set; }
        public List<BandViewModel> BandList = new List<BandViewModel>();
        public List<ConcertzaalViewModel> ConcertzaalList = new List<ConcertzaalViewModel>();

    }
}
