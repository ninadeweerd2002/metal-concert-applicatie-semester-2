﻿namespace Metal_Webapp.Models
{
    public class Library
    {
        public int BandModelID { get; set; }
        public List<BandViewModel> BandList = new List<BandViewModel>();
    }
}
