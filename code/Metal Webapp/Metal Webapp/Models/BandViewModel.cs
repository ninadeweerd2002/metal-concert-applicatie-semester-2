﻿using DAL;
using BLL;
using System.ComponentModel.DataAnnotations;

namespace Metal_Webapp.Models
{
    public class BandViewModel
    {
        public int BandID { get; set; }

        [Required(ErrorMessage = " Vul dit veld in!")]
        [RegularExpression(@"([A-Za-z])+( [A-Za-z]+)*", ErrorMessage = " Alleen letters gebruiken!")]
        public string BandNaam { get; set; }
        public string BandInfo { get; set; }

        public BandViewModel(string bandnaam)
        {
            BandNaam = bandnaam;
        }

        public BandViewModel(BandModel b)
        {
            BandID = b.BandID;
            BandNaam = b.BandNaam;
            BandInfo = b.BandInfo;
            
        }

        public BandViewModel(int bandid)
        {
            BandID = bandid;
        }

        public BandViewModel()
        {
        }
    }
}
