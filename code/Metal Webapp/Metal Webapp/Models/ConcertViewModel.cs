﻿using DAL;
using BLL;
using System.ComponentModel.DataAnnotations;

namespace Metal_Webapp
{
    public class ConcertViewModel
    {
        public int ID { get; set; }
        public int BandID { get; set; }

        [Required(ErrorMessage = " Vul dit veld in!")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = " Alleen letters gebruiken!")]
        public string BandNaam { get; set; }
        public string BandInfo { get; set; }
        public int ConcertzaalID { get; set; }

        [Required(ErrorMessage = " Vul dit veld in!")]
        [RegularExpression(@"([A-Za-z])+( [A-Za-z]+)*", ErrorMessage = " Alleen letters gebruiken!")]
        public string ConcertzaalNaam { get; set; }
        public string Locatie { get; set; }
        public string Land { get; set; }

        [Required(ErrorMessage = " Vul dit veld in!")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Datum { get; set; }

        [Required(ErrorMessage = " Vul dit veld in!")]
        public string BeginTijd { get; set; }

        [Required(ErrorMessage = " Vul dit veld in!")]
        public string EindTijd { get; set; }

        public ConcertViewModel(ConcertModel c)
        {
            ID = c.ID;
            BandID = c.BandID;
            BandNaam = c.BandNaam;
            BandInfo = c.BandInfo;
            ConcertzaalID = c.ConcertzaalID;
            ConcertzaalNaam = c.ConcertzaalNaam;
            Locatie = c.Locatie;
            Land = c.Land;
            Datum = c.Datum;
            BeginTijd = c.BeginTijd;
            EindTijd = c.EindTijd;
        }

        public ConcertViewModel()
        {
        }

        public ConcertViewModel(int id)
        {
            ID = id;
        }
        public ConcertViewModel(int id, DateTime datum, string begintijd, string eindtijd)
        {
            ID = id;
            Datum = datum;
            BeginTijd = begintijd;
            EindTijd = eindtijd;
        }
    }
}
