﻿using Metal_Webapp.Models;
namespace Metal_Webapp
{
    public class RegBand
    {
        public int BandModelID { get; set; }
        public string Gebruikersnaam { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public List<BandViewModel> BandList = new List<BandViewModel>();
    }
}
