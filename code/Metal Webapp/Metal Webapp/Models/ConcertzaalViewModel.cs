﻿using DAL;
using BLL;
using System.ComponentModel.DataAnnotations;

namespace Metal_Webapp
{
    public class ConcertzaalViewModel
    {
        public int ConcertzaalID { get; set; }

        [Required(ErrorMessage = " Vul dit veld in!")]
        [RegularExpression(@"([A-Za-z])+( [A-Za-z]+)*", ErrorMessage = " Alleen letters gebruiken!")]
        public string ConcertzaalNaam { get; set; }

        [Required(ErrorMessage = " Vul dit veld in!")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = " Alleen letters gebruiken!")]
        public string Locatie { get; set; }

        [Required(ErrorMessage = " Vul dit veld in!")]
        public string Land { get; set; }

        public ConcertzaalViewModel(int concertzaalid, string concertzaalnaam, string locatie, string land)
        {
            ConcertzaalID = concertzaalid;
            ConcertzaalNaam = concertzaalnaam;
            Locatie = locatie;
            Land = land;
        }

        public ConcertzaalViewModel()
        {

        }

        public ConcertzaalViewModel(ConcertzaalModel c)
        {
            ConcertzaalID = c.ConcertzaalID;
            ConcertzaalNaam = c.ConcertzaalNaam;
            Locatie = c.Locatie;
            Land = c.Land;
        }
        public ConcertzaalViewModel(int concertzaalid)
        {
            ConcertzaalID = concertzaalid;
        }
    }
}
