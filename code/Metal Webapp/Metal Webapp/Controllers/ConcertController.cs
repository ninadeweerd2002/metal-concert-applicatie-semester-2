﻿using Microsoft.AspNetCore.Mvc;
using DAL;
using BLL;
using Metal_Webapp.Models;

namespace Metal_Webapp
{
    public class ConcertController : Controller
    {

        public ConcertContainer container = new ConcertContainer(new ConcertContext());
        private ConcertzaalContainer containerConcertzaal = new ConcertzaalContainer(new ConcertzaalContext());
        private BandContainer containerBand = new BandContainer(new BandContext());
        private ConvertConcert convert = new ConvertConcert();
        private ConvertBand convertBand = new ConvertBand();
        private ConvertConcertzaal convertConcertzaal = new ConvertConcertzaal();

        public IActionResult Index()
        {
            // Show all concerten

            List<ConcertViewModel> concerts = container.GetAll().Select(convert.ConvertToConcertViewModel).ToList();

            return View(concerts);

        }
        public IActionResult Admin()
        {
            // Access for admin only

            if (HttpContext.Session == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (HttpContext.Session.GetString("username") != "admin")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                // Show all concerten

                List<ConcertViewModel> concerts = container.GetAll().Select(convert.ConvertToConcertViewModel).ToList();

                return View(concerts);
            }
        }

        [HttpGet]
        public IActionResult Concertaanmaken()
        {
            if (HttpContext.Session == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (HttpContext.Session.GetString("username") != "admin")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                // Get all concerten, bands and concertzalen

                List<BandViewModel> bands = containerBand.GetAll().Select(convertBand.ConvertToBandViewModel).ToList();
                List<ConcertzaalViewModel> concertzalen = containerConcertzaal.GetAll().Select(convertConcertzaal.ConvertToConcertzaalViewModel).ToList();

                // Add everything to AllClasses

                AllClasses model = new AllClasses();
                model.BandList = bands;
                model.ConcertzaalList = concertzalen;

                return View(model);
            }
        }
        [HttpPost]
        public IActionResult Concertaanmaken(AllClasses c)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    // Add concert

                    ConcertViewModel concert = new ConcertViewModel() { BandID = c.BandModelID, ConcertzaalID = c.ConcertzaalModelID, Datum = c.Datum, BeginTijd = c.BeginTijd, EindTijd = c.EindTijd };
                    container.AddConcert(convert.ConvertConcertModel(concert));
                 
                    TempData["Succes"] = "Concert is aangemaakt!";
                    
                    return RedirectToAction("Admin");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return View(c);
        }
        public IActionResult DeleteConcert(int id)
        {
            if (ModelState.IsValid)
            {
                // Delete concert

                container.DeleteConcert(container.GetByID(id));

                TempData["Succes"] = "Concert is verwijderd!";
                return RedirectToAction("Admin");
            }
            return View();
        }

        public IActionResult Gegevens(int id)
        {
            ConcertViewModel concerts = convert.ConvertToConcertViewModel(container.GetByID(id));
    
            return View(concerts);

        }
        [HttpGet]
        public IActionResult Concertaanpassen(int id)
        {
            if (HttpContext.Session == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (HttpContext.Session.GetString("username") != "admin")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                // Get concert by id

                ConcertViewModel concerts = convert.ConvertToConcertViewModel(container.GetByID(id));

                return View(concerts);
            }
        }

        [HttpPost]
        public IActionResult Concertaanpassen(ConcertViewModel c)
        {
            ModelState.Remove("banddto");
            ModelState.Remove("BandNaam");
            ModelState.Remove("BandInfo");
            ModelState.Remove("ConcertzaalNaam");
            ModelState.Remove("Locatie");
            ModelState.Remove("Land");
            try
            {
                if (ModelState.IsValid)
                {
                    // Update selected concert

                    container.UpdateConcert(convert.ConvertConcertModel(c));
                    TempData["Succes"] = "Concert is aangepast!";
                    return RedirectToAction("Admin");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return View();
        }
    }  
}
