﻿using Microsoft.AspNetCore.Mvc;
using DAL;
using BLL;
using System.Data;
using Metal_Webapp.Models;
using Microsoft.AspNetCore.Routing;
using System.Web;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Metal_Webapp
{
    public class GebruikerController : Controller
    {
        private GebruikerContainer container = new GebruikerContainer(new GebruikerContext());
        private BandContainer containerBand = new BandContainer(new BandContext());
        private ConvertGebruiker convert = new ConvertGebruiker();
        private ConvertBand convertBand = new ConvertBand();
        

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Admin()
        {
            // Admin access only

            if (HttpContext.Session == null)
            {
                return RedirectToAction("Index", "Home");
            } else if (HttpContext.Session.GetString("username") != "admin")
            {
                return RedirectToAction("Index", "Home");
            }else
            {
                // Get all gebruikers

                List<GebruikerViewModel> gebruikers = container.GetAll().Select(convert.ConvertToGebruikerViewModel).ToList();
                return View(gebruikers);
            }    
        }
        [HttpPost]
        public IActionResult Index(GebruikerModel g)
        {
            ModelState.Remove("Email");
            ModelState.Remove("BandNaam");
            if (ModelState.IsValid)
            {
                GebruikerModel gebruiker = new GebruikerModel() { ID = g.ID, Gebruikersnaam = g.Gebruikersnaam, Password = g.Password, BandID = g.BandID};
                int id = container.Inloggen(gebruiker);

                // Get ID, Gebruikersnaam and Password

                if (id != 0)
                {

                    if (gebruiker.Gebruikersnaam == "admin")
                    {
                        // Admin login

                        HttpContext.Session.SetInt32("id", id);
                        HttpContext.Session.SetString("username", gebruiker.Gebruikersnaam);

                        return RedirectToAction("Index", "Admin");
                    }
                    else
                    {
                        // Non-admin login
                        HttpContext.Session.SetInt32("id", id);
                        HttpContext.Session.SetString("username", gebruiker.Gebruikersnaam);
                        return RedirectToAction("Profiel", new {ID = id});
                    }
                    
                } else
                {
                    TempData["Error"] = "Gebruikersnaam of wachtwoord onjuist";
                }
        
            }

            return View();
        }

        [HttpGet]
        public IActionResult Profiel(int id)
        {
            ModelState.Remove("BandNaam");
            if (HttpContext.Session == null)
            {
                return RedirectToAction("Index", "Gebruiker");
            }
            else if (HttpContext.Session.GetInt32("id") != id)
            {
                return RedirectToAction("Index", "Gebruiker");
            }
            else
            {
                

                // Get gebruiker by id

                GebruikerViewModel gebruikers = convert.ConvertToGebruikerViewModel(container.GetByID(id));
                List<GebruikerViewModel> bandlib = container.BandLibrary(id).Select(convert.ConvertToGebruikerViewModel).ToList();

                // Get all bands

                List<BandViewModel> bands = containerBand.GetAll().Select(convertBand.ConvertToBandViewModel).ToList();
                

                BandLibrary model = new BandLibrary();

                // Add bands and gebruiker to BandLibrary

                model.GebruikerList = bandlib;
                model.Gebruiker = gebruikers;
                model.BandList = bands;

                return View(model);
            }
        }
        public IActionResult Uitloggen()
        {
            // Gebruiker logout

            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }      

        [HttpGet]
        public IActionResult Registreren()
        {
            // Get all bands

            List<BandViewModel> bands = containerBand.GetAll().Select(convertBand.ConvertToBandViewModel).ToList();
    
            RegBand model = new RegBand();
            model.BandList = bands;

            return View(model);
        }
    
    [HttpPost]
        public IActionResult Registreren(RegBand c)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    GebruikerViewModel gebruiker = new GebruikerViewModel() { Gebruikersnaam = c.Gebruikersnaam, Email = c.Email, Password = c.Password, BandID = c.BandModelID };
                    container.AddGebruiker(convert.ConvertGebruikerModel(gebruiker));
                    TempData["Success"] = "Account is aangemaakt!";
                    return RedirectToAction("Index");
                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return View();
        }

        [HttpGet]
        public IActionResult Gebruikeraanpassen(int id)
        {
            if (HttpContext.Session == null)
            {
                return RedirectToAction("Index", "Gebruiker");
            }
            else if (HttpContext.Session.GetInt32("id") != id)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {

                // Get gebruiker by id

                GebruikerViewModel gebruikers = convert.ConvertToGebruikerViewModel(container.GetByID(id));

                return View(gebruikers);
            }
        }
        
        [HttpPost]
        public IActionResult Gebruikeraanpassen(GebruikerViewModel g)
        {
            ModelState.Remove("BandNaam");

            if (ModelState.IsValid)
            {
                // Update selected gebruiker

                container.UpdateGebruiker(convert.ConvertGebruikerModel(g));
                return RedirectToAction("Profiel");
            }
            return View();


        }
        
        public IActionResult DeleteGebruiker(int id)
        {
            if (ModelState.IsValid)
            {
                // Delete selected gebruiker
               
                container.DeleteGebruiker(container.GetByID(id));

                return RedirectToAction("Admin");
            }
            return View();
        }
        [HttpGet]
        public IActionResult AddBandToLibrary(int id)
        {
            if (HttpContext.Session == null)
            {
                return RedirectToAction("Index", "Gebruiker");
            }
            else if (HttpContext.Session.GetInt32("id") != id)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                GebruikerViewModel gebruikers = convert.ConvertToGebruikerViewModel(container.GetByID(id));
                List<BandViewModel> bandjes = containerBand.GetAll().Select(convertBand.ConvertToBandViewModel).ToList();

                HttpContext.Session.SetInt32("idgebruiker", gebruikers.ID);
                Library model = new Library();
                model.BandList = bandjes;

                return View(model);
            }
        }

        [HttpPost]
        public IActionResult AddBandToLibrary(Library c)
        {
            try
            {
                if (ModelState.IsValid)
                {   
                    
                    GebruikerViewModel gebruiker = new GebruikerViewModel() { BandID = c.BandModelID, GebruikerID = Convert.ToInt32(HttpContext.Session.GetInt32("idgebruiker"))};
                    container.AddBandToLibrary(convert.ConvertGebruikerModel(gebruiker));
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return View();
        }
        public IActionResult DeleteBandFromLibrary(int id)
        {
            if (ModelState.IsValid)
            {
                // Delete selected gebruiker               
                container.DeleteBandFromLibrary(container.GetByIDKoppeltable(id));

                return RedirectToAction("Index", "Home");
            }
            return View();
        }
    }
}
