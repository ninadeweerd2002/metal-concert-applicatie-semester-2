﻿using Microsoft.AspNetCore.Mvc;
using DAL;
using Metal_Webapp.Models;
using BLL;

namespace Metal_Webapp.Controllers
{
    public class ConcertzaalController : Controller
    {
        private ConcertzaalContainer containerConcertzaal = new ConcertzaalContainer(new ConcertzaalContext());
        private ConcertContainer containerConcert = new ConcertContainer(new ConcertContext());
        private ConvertConcertzaal convert = new ConvertConcertzaal();
        private ConvertConcert convertConcert = new ConvertConcert();


        public IActionResult Index()
        {
            // Get all concertzalen

            List<ConcertzaalViewModel> concertzalen = containerConcertzaal.GetAll().Select(convert.ConvertToConcertzaalViewModel).ToList();
            return View(concertzalen);

        }

        public IActionResult Gegevens(int id)
        {
            BandLibrary bl = new BandLibrary();

            // Get info concertzaal by id

            ConcertzaalViewModel concertzalen = convert.ConvertToConcertzaalViewModel(containerConcertzaal.GetByID(id));

            // Get all concerten by selected concertzaal

            List<ConcertViewModel> concerts = containerConcert.GetConcertsByConcertzaal(id).Select(convertConcert.ConvertToConcertViewModel).ToList();


            bl.ConcertzaalInfo = concertzalen;
            bl.ConcertList = concerts;

            return View(bl);

        }
        public IActionResult Admin()
        {
            // Admin access only

            if (HttpContext.Session == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (HttpContext.Session.GetString("username") != "admin")
            {
                return RedirectToAction("Admin", "Home");
            }
            else
            {
                // Get all concertzalen

                List<ConcertzaalViewModel> concertzalen = containerConcertzaal.GetAll().Select(convert.ConvertToConcertzaalViewModel).ToList();
                return View(concertzalen);
            }
        }

        public IActionResult DeleteConcertzaal(int id)
        {
            if (ModelState.IsValid)
            {
                // Delete concertzaal selected by id

                containerConcertzaal.DeleteConcertzaal(containerConcertzaal.GetByID(id));
                TempData["Succes"] = "Concertzaal is verwijderd!";
                return RedirectToAction("Admin");
            }
            return View();
        }
        [HttpGet]
        public IActionResult Concertzaalaanpassen(int id)
        {
            if (HttpContext.Session == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (HttpContext.Session.GetString("username") != "admin")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                // Select concertzaal by id

                ConcertzaalViewModel concertzalen = convert.ConvertToConcertzaalViewModel(containerConcertzaal.GetByID(id));

                return View(concertzalen);
            }
        }

        [HttpPost]
        public IActionResult Concertzaalaanpassen(ConcertzaalViewModel concertzaal)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Update selected concertzaal

                    containerConcertzaal.UpdateConcertzaal(convert.ConvertConcertzaalModel(concertzaal));
                    TempData["Succes"] = "Concertzaal is aangepast!";
                    return RedirectToAction("Admin");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return View();
        }
        [HttpGet]
        public IActionResult Concertzaalaanmaken()
        {
            if (HttpContext.Session == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (HttpContext.Session.GetString("username") != "admin")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public IActionResult Concertzaalaanmaken(ConcertzaalViewModel cz)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Add new concertzaal

                    containerConcertzaal.AddConcertzaal(convert.ConvertConcertzaalModel(cz));
                    TempData["Succes"] = "Concertzaal is aangemaakt!";
                    return RedirectToAction("Admin");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return View();
        }

    }
}

