﻿using Metal_Webapp.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using BLL;
using DAL;

namespace Metal_Webapp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;      
        public ConcertContainer container = new ConcertContainer(new ConcertContext());      

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            List<ConcertViewModel> concert = new List<ConcertViewModel>();

            var concerten = from c in container.GetAll().Select(model => new ConcertViewModel(model))
                .ToList() select c;
            int count = concerten.Count();
            int id = new Random().Next(count);
            var randomConcert = concerten.Skip(id).FirstOrDefault();

            ViewBag.Message = HttpContext.Session.GetString("username");

            return View(randomConcert);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}