﻿using Microsoft.AspNetCore.Mvc;
using BLL;
using DAL;
using Metal_Webapp.Models;

namespace Metal_Webapp
{
    public class BandController : Controller
    {
        private BandContainer containerBand = new BandContainer(new BandContext());
        private ConcertContainer containerConcert = new ConcertContainer(new ConcertContext());
        private ConvertBand convert = new ConvertBand();
        private ConvertConcert convertConcert = new ConvertConcert();


        public IActionResult Index()
        {
            // Show all bands

            List<BandViewModel> bands = containerBand.GetAll().Select(convert.ConvertToBandViewModel).ToList();

            return View(bands);
        }

        // Access for admin only
        public IActionResult Admin()
        {
            if (HttpContext.Session == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (HttpContext.Session.GetString("username") != "admin")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                // Show all bands

                List<BandViewModel> bands = containerBand.GetAll().Select(convert.ConvertToBandViewModel).ToList();

                return View(bands);
            }
        }
        public IActionResult DeleteBand(int id)
        {
            if (ModelState.IsValid)
            {
                // Delete band by id

                containerBand.DeleteBand(containerBand.GetByID(id));
                TempData["Succes"] = "Band is verwijderd!";
                return RedirectToAction("Admin");
            }
            return View();
        }

        [HttpGet]
        public IActionResult Bandaanmaken()
        {
            if (HttpContext.Session == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (HttpContext.Session.GetString("username") != "admin")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {           
                return View();
            }
        }

        [HttpPost]
        public IActionResult Bandaanmaken(BandViewModel b)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Add new band

                    containerBand.AddBand(convert.ConvertBandModel(b));
                    TempData["Succes"] = "Band is aangemaakt!";
                    return RedirectToAction("Admin");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return View();
        }
        [HttpGet]
        public IActionResult Bandaanpassen(int id)
        {
            // Get band by selected id
            if (HttpContext.Session == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (HttpContext.Session.GetString("username") != "admin")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                BandViewModel bands = convert.ConvertToBandViewModel(containerBand.GetByID(id));

                return View(bands);
            }
        }

        [HttpPost]
        public IActionResult Bandaanpassen(BandViewModel b)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Edit selected band

                    containerBand.UpdateBand(convert.ConvertBandModel(b));
                    TempData["Succes"] = "Band is aangepast!";
                    return RedirectToAction("Admin");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return View();
        }
        public IActionResult Gegevens(int id)
        {
            BandLibrary bl = new BandLibrary();

            // Show info from selected band

            BandViewModel bands = convert.ConvertToBandViewModel(containerBand.GetByID(id));

            // Get all concerts from selected band

            List<ConcertViewModel> concerts = containerConcert.GetConcertsByBand(id).Select(convertConcert.ConvertToConcertViewModel).ToList();


            // Add bands and concerts to BandLibrary

            bl.Band = bands;
            bl.ConcertList = concerts;

            return View(bl);

        }
    }
}
