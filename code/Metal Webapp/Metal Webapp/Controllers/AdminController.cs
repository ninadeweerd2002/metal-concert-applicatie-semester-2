﻿using Microsoft.AspNetCore.Mvc;

namespace Metal_Webapp
{
    public class AdminController : Controller
    {
        public IActionResult Index()
        {
            // Access for admin only

            if (HttpContext.Session == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (HttpContext.Session.GetString("username") != "admin")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }
    }
}
