﻿using DAL;
using System.Data.SqlClient;
namespace BLL
{
    public class BandContainer
    {
        private IBandContext context;
        public BandContainer(IBandContext _context)
        {
            context = _context;
        }
        public void AddBand(BandModel band)
        {       
            BandDTO dto = new BandDTO(band.BandID, band.BandNaam, band.BandInfo);
            context.AddBand(dto);
        }

        public void UpdateBand(BandModel band)
        {
            BandDTO dto = new BandDTO(band.BandID, band.BandNaam, band.BandInfo);
            context.UpdateBand(dto);

        }
        public void DeleteBand(BandModel band)
        {
            BandDTO dto = new BandDTO(band.BandID, band.BandNaam, band.BandInfo);
            context.DeleteBand(dto);

        }

        public BandModel GetByID(int id)
        {
            BandModel c = new BandModel(context.GetByID(id));
            return c;
        }
        public List<BandModel> GetAll()
        {

            List<BandDTO> dtoList = context.GetAll();

            List<BandModel> bands = new List<BandModel>();
            foreach (var item in dtoList)
            {
                bands.Add(new BandModel(item));
            }

            return bands;
        }

        
    }
}
