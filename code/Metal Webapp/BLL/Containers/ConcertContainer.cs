﻿using DAL;
using System.Data.SqlClient;
namespace BLL
{
    public class ConcertContainer
    {
        private IConcertContext context;
        public ConcertContainer(IConcertContext _context)
        {
            context = _context;
        }
        public void AddConcert(ConcertModel concert)
        {
            ConcertDTO dto = new ConcertDTO(concert.ID, concert.BandID, concert.ConcertzaalID, concert.Datum, concert.BeginTijd, concert.EindTijd);
            context.AddConcert(dto);
        }

        public void DeleteConcert(ConcertModel concert)
        {
            ConcertDTO dto = new ConcertDTO(concert.ID, concert.BandID, concert.ConcertzaalID, concert.Datum, concert.BeginTijd, concert.EindTijd);
            context.DeleteConcert(dto);

        }
        public void UpdateConcert(ConcertModel concert)
        {
            ConcertDTO dto = new ConcertDTO(concert.ID, concert.BandID, concert.ConcertzaalID, concert.Datum, concert.BeginTijd, concert.EindTijd);
            context.UpdateConcert(dto);
        }
        public ConcertModel GetByID(int id)
        {
            ConcertModel c = new ConcertModel(context.GetByID(id));
            return c;
        }
        public List<ConcertModel> GetAll()
        {

            List<ConcertDTO> dtoList = context.GetAll();

            List<ConcertModel> concerten = new List<ConcertModel>();

            foreach (var item in dtoList)
            {
                concerten.Add(new ConcertModel(item));
            }

            return concerten;
        }
        public List<ConcertModel> GetConcertsByBand(int id)
        {

            List<ConcertModel> bands = new List<ConcertModel>();
            foreach (ConcertDTO item in context.GetConcertsByBand(id))
            {               
                bands.Add(new ConcertModel(item));
            }

            return bands;
        }

        public List<ConcertModel> GetConcertsByConcertzaal(int id)
        {

            List<ConcertModel> concertzalen = new List<ConcertModel>();
            foreach (ConcertDTO item in context.GetConcertsByConcertzaal(id))
            {
                concertzalen.Add(new ConcertModel(item));
            }

            return concertzalen;
        }
    }
}