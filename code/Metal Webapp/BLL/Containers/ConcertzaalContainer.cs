﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class ConcertzaalContainer
    {
        private IConcertzaalContext context;
        public ConcertzaalContainer(IConcertzaalContext _context)
        {
            context = _context;
        }
        public void AddConcertzaal(ConcertzaalModel concertzaal)
        {
            ConcertzaalDTO dto = new ConcertzaalDTO(concertzaal.ConcertzaalID, concertzaal.ConcertzaalNaam, concertzaal.Locatie, concertzaal.Land);
            context.AddConcertzaal(dto);
        }

        public void DeleteConcertzaal(ConcertzaalModel concertzaal)
        {
            ConcertzaalDTO dto = new ConcertzaalDTO(concertzaal.ConcertzaalID, concertzaal.ConcertzaalNaam, concertzaal.Locatie, concertzaal.Land);
            context.DeleteConcertzaal(dto);

        }
        public void UpdateConcertzaal(ConcertzaalModel concertzaal)
        {
            ConcertzaalDTO dto = new ConcertzaalDTO(concertzaal.ConcertzaalID, concertzaal.ConcertzaalNaam, concertzaal.Locatie, concertzaal.Land);
            context.UpdateConcertzaal(dto);
        }
    
        public List<ConcertzaalModel> GetAll()
        {

            List<ConcertzaalDTO> dtoList = context.GetAll();

            List<ConcertzaalModel> concertzlijst = new List<ConcertzaalModel>();

            foreach (var item in dtoList)
            {
                concertzlijst.Add(new ConcertzaalModel(item));
            }

            return concertzlijst;
        }
        public ConcertzaalModel GetByID(int id)
        {
            ConcertzaalModel c = new ConcertzaalModel(context.GetByID(id));
            return c;
        }
    }
}
