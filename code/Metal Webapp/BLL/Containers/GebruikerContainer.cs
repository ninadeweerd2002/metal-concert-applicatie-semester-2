﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class GebruikerContainer
    {
        private IGebruikerContext context;
        public GebruikerContainer(IGebruikerContext _context)
        {
            context = _context;
        }
        public void AddGebruiker(GebruikerModel gebruiker)
        {
            GebruikerDTO dto = new GebruikerDTO(gebruiker.ID, gebruiker.Gebruikersnaam, gebruiker.Email, gebruiker.Password, gebruiker.BandID);
            context.AddGebruiker(dto);
        }
        public void UpdateGebruiker(GebruikerModel gebruiker)
        {
            GebruikerDTO dto = new GebruikerDTO(gebruiker.ID, gebruiker.Gebruikersnaam, gebruiker.Email, gebruiker.Password, gebruiker.BandID);
            context.UpdateGebruiker(dto);
        }
        public void DeleteGebruiker(GebruikerModel gebruiker)
        {
            GebruikerDTO dto = new GebruikerDTO(gebruiker.ID, gebruiker.Gebruikersnaam, gebruiker.Email, gebruiker.Password, gebruiker.BandID);
            context.DeleteGebruiker(dto);
        }

        

        public int Inloggen(GebruikerModel gebruiker)
        {
            GebruikerDTO dto = new GebruikerDTO(gebruiker.ID, gebruiker.Gebruikersnaam, gebruiker.Password);
            return context.Inloggen(dto);
            
        }

        public List<GebruikerModel> GetAll()
        {

            List<GebruikerDTO> dtoList = context.GetAll();

            List<GebruikerModel> concertzlijst = new List<GebruikerModel>();

            foreach (var item in dtoList)
            {
                concertzlijst.Add(new GebruikerModel(item));
            }

            return concertzlijst;
        }


        public GebruikerModel GetByID(int id)
        {
            GebruikerModel c = new GebruikerModel(context.GetByID(id));
            return c;
        }
        public GebruikerModel GetByIDKoppeltable(int id)
        {
            GebruikerModel c = new GebruikerModel(context.GetByIDKoppeltable(id));
            return c;
        }

        public void AddBandToLibrary(GebruikerModel gebruiker)
        {
            GebruikerDTO dto = new GebruikerDTO(gebruiker.ID, gebruiker.GebruikerID, gebruiker.BandID);
            context.AddBandToLibrary(dto);
        }

        public void DeleteBandFromLibrary(GebruikerModel gebruiker)
        {
            GebruikerDTO dto = new GebruikerDTO(gebruiker.ID, gebruiker.GebruikerID, gebruiker.BandID);
            context.DeleteBandFromLibrary(dto);
        }

        public List<GebruikerModel> BandLibrary(int id)
        {

            List<GebruikerModel> gebruiker = new List<GebruikerModel>();
            foreach (GebruikerDTO item in context.BandLibrary(id))
            {
                gebruiker.Add(new GebruikerModel(item));
            }

            return gebruiker;
        }
    }
}
