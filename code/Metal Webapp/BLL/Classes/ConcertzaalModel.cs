﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class ConcertzaalModel
    {
        public int ConcertzaalID { get; set; }
        public string ConcertzaalNaam { get; set; }    
        public string Locatie { get; set; }
        public string Land { get; set; }

        public ConcertzaalModel(ConcertzaalDTO cz)
        {
            ConcertzaalID = cz.ConcertzaalID;
            ConcertzaalNaam = cz.ConcertzaalNaam;
            Locatie = cz.Locatie;
            Land = cz.Land;
        }

        public ConcertzaalModel()
        {
        }

        public ConcertzaalModel(int id)
        {
            ConcertzaalID = id;
        }
        public ConcertzaalModel(int id, string concertzaalnaam, string locatie, string land)
        {
            ConcertzaalID = id;
            ConcertzaalNaam = concertzaalnaam;
            Locatie = locatie;
            Land = land;
        }
        public ConcertzaalModel(string concertzaalnaam, string locatie, string land)
        {
            ConcertzaalNaam = concertzaalnaam;
            Locatie = locatie;
            Land = land;
        }
    }
}
