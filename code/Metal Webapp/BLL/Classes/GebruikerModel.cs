﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class GebruikerModel
    {
        public int ID { get; set; }
        public string Gebruikersnaam { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int GebruikerID { get; set; }
        public int BandID { get; set; }
        public string BandNaam { get; set; }


        public GebruikerModel(GebruikerDTO c)
        {
            ID = c.ID;
            Gebruikersnaam = c.Gebruikersnaam;
            Email = c.Email;
            Password = c.Password;
            GebruikerID = c.GebruikerID;
            BandID = c.BandID;
            BandNaam = c.BandDTO.BandNaam;
            
        }

        public GebruikerModel(string gb, string email, string pw, int bandid)
        {
            Gebruikersnaam = gb;
            Email = email;
            Password = pw;
            BandID = bandid;

        }

        public GebruikerModel(int id, string gb, string email, string pw, string bandnaam)
        {
            ID = id;
            Gebruikersnaam = gb;
            Email = email;
            Password = pw;
            BandNaam = bandnaam;

        }
        public GebruikerModel(int id, string gb, string pw)
        {
            ID = id;
            Gebruikersnaam = gb;
            Password = pw;

        }

        public GebruikerModel(int id, string gb, string email, string pw)
        {
            ID = id;
            Gebruikersnaam = gb;
            Email = email;
            Password = pw;

        }

        public GebruikerModel()
        {
        }

        public GebruikerModel(int id, int gebruikerid, int bandid)
        {
            ID = id;
            GebruikerID = gebruikerid;
            BandID = bandid;
        }
        public GebruikerModel(int id, int gebruikerid, string gb, string email, string pw, int bandID, string bandnaam)
        {
            ID = id;
            Gebruikersnaam = gb;
            Email = email;
            Password = pw;
            GebruikerID = gebruikerid;
            BandID = bandID;
            BandNaam = bandnaam;
        }
    }
}
