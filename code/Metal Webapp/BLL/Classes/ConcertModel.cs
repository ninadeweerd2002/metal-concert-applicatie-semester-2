﻿using DAL;
using System.ComponentModel.DataAnnotations;

namespace BLL
{
    public class ConcertModel
    {
        public int ID { get; set; }
        public int BandID { get; set; }
        public string BandNaam { get; set; }
        public string BandInfo { get; set; }
        public int ConcertzaalID { get; set; }
        public string ConcertzaalNaam { get; set; }
        public string Locatie { get; set; }
        public string Land { get; set; }
        public DateTime Datum { get; set; }
        public string BeginTijd { get; set; }
        public string EindTijd { get; set; }


        public ConcertModel(ConcertDTO c)
        {
            ID = c.ID;
            BandID = c.BandID;
            BandNaam = c.BandDTO.BandNaam;
            BandInfo = c.BandDTO.BandInfo;
            ConcertzaalID = c.ConcertzaalID;
            ConcertzaalNaam = c.ConcertzaalDTO.ConcertzaalNaam;
            Locatie = c.ConcertzaalDTO.Locatie;
            Land = c.ConcertzaalDTO.Land;
            Datum = c.Datum;
            BeginTijd = c.BeginTijd;
            EindTijd = c.EindTijd;
        }

        public ConcertModel()
        {
        }

        public ConcertModel(int id)
        {
            ID = id;
        }
        public ConcertModel(int id, DateTime datum, string begintijd, string eindtijd)
        {
            ID = id;
            Datum = datum;
            BeginTijd = begintijd;
            EindTijd = eindtijd;
        }
        public ConcertModel(int id, string bandnaam, string concertzaal, DateTime datum, string begintijd, string eindtijd)
        {
            ID = id;
            BandNaam = bandnaam;
            ConcertzaalNaam = concertzaal;
            Datum = datum;
            BeginTijd = begintijd;
            EindTijd = eindtijd;
        }

        public ConcertModel(int id, int bandid, string bandnaam, string bandinfo, int concertzaalid, string concertzaalnaam, string locatie, string land, DateTime datum, string begintijd, string eindtijd)
        {
            ID = id;
            BandID = bandid;
            BandNaam = bandnaam;
            BandInfo = bandinfo;
            ConcertzaalID = concertzaalid;
            ConcertzaalNaam = concertzaalnaam;
            Locatie = locatie;
            Land = land;
            Datum = datum;
            BeginTijd = begintijd;
            EindTijd = eindtijd;
        }
    }
}
