﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class BandModel
    {
        public int BandID { get; set; }
        public string BandNaam { get; set; }
        public string BandInfo { get; set; }

        public BandModel(string bandnaam)
        {
            BandNaam = bandnaam;
        }

        public BandModel(BandDTO b)
        {
            BandID = b.BandID;
            BandNaam = b.BandNaam;
            BandInfo = b.BandInfo;
        }

        public BandModel(int bandid)
        {
            BandID = bandid;
        }
        public BandModel(int bandid, string bandnaam, string bandinfo)
        {
            BandID = bandid;
            BandNaam = bandnaam;
            BandInfo = bandinfo;
        }

        public BandModel()
        {
        }
    }
}
