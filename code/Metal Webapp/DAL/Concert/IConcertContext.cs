﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IConcertContext
    {
        public void AddConcert(ConcertDTO concert);
        public ConcertDTO GetByID(int id);
        public void DeleteConcert(ConcertDTO concert);
        public void UpdateConcert(ConcertDTO concert);
        public List<ConcertDTO> GetAll();
        public List<ConcertDTO> GetConcertsByBand(int id);
        public List<ConcertDTO> GetConcertsByConcertzaal(int id);

    }
}
