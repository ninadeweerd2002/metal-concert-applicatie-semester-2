﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace DAL
{
    public class ConcertDTO
    {
        public int ID { get; set; }
        public int BandID { get; set; }
        public int ConcertzaalID { get; set; }      
        public BandDTO BandDTO { get; set; }
        public ConcertzaalDTO ConcertzaalDTO { get; set; }
        public DateTime Datum { get; set; }
        public string BeginTijd { get; set; }
        public string EindTijd { get; set; }


       

        public ConcertDTO(int id, int bandId, BandDTO bandnaam, ConcertzaalDTO concertzaal, int concertzaalId, DateTime datum, string bTijd, string eTijd)
        {
            this.ID = id;
            this.BandID = bandId;
            this.BandDTO = bandnaam;
            this.ConcertzaalID = concertzaalId;
            this.ConcertzaalDTO = concertzaal;
            this.Datum = datum;
            this.BeginTijd = bTijd;
            this.EindTijd = eTijd;
        }

        public ConcertDTO(int id, int bandId, int concertzaalId, DateTime datum, string bTijd, string eTijd)
        {
            this.ID = id;
            this.BandID = bandId;
            this.ConcertzaalID = concertzaalId;
            this.Datum = datum;
            this.BeginTijd = bTijd;
            this.EindTijd = eTijd;
        }
        public ConcertDTO(int id, DateTime datum, string bTijd, string eTijd)
        {
            this.ID = id;
            this.Datum = datum;
            this.BeginTijd = bTijd;
            this.EindTijd = eTijd;
        }


        public ConcertDTO()
        {
        }

    }
}
