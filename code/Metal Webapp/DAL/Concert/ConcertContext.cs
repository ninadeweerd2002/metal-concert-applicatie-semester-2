﻿using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class ConcertContext : IConcertContext
    {
        private string conn = "Data Source=LAPTOP-4LVGLOHE;Initial Catalog=Fontys metal;Integrated Security=True"; // - LAPTOP LOKALE DATABASE
        //private string conn = "Data Source=DESKTOP-HOADRGG;Initial Catalog=Fontys metal;Integrated Security=True"; // - DESKTOP LOKALE DATABASE
        private string query;


        public void UpdateConcert(ConcertDTO concert)
        {
            query = "UPDATE concerten SET datum = @datum, tijdbegin = @tijdbegin, tijdeinde = @tijdeinde WHERE id = @id";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@id", concert.ID);
                        q.Parameters.AddWithValue("@datum", concert.Datum);
                        q.Parameters.AddWithValue("@tijdbegin", concert.BeginTijd);
                        q.Parameters.AddWithValue("@tijdeinde", concert.EindTijd);
                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
        public ConcertDTO GetByID(int id)
        {
            query = @"SELECT * FROM concerten  
                        JOIN bands 
                          ON bands.idband=concerten.idband
                        JOIN concertzaal 
                          ON concertzaal.idconcertzaal=concerten.idconcertzaal
                       WHERE id=@id";

            ConcertDTO dto = new ConcertDTO();
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@id", id);

                        SqlDataReader r = q.ExecuteReader();

                        while (r.Read())
                        {
                            dto.ID = Convert.ToInt32(r["id"]);
                            dto.BandID = Convert.ToInt32(r["idband"]);
                            dto.BandDTO = new BandDTO() { 
                                BandID = Convert.ToInt32(r["idband"]), 
                                BandNaam = r["bandnaam"].ToString(), 
                                BandInfo = r["infoband"].ToString() 
                            };
                            dto.ConcertzaalDTO = new ConcertzaalDTO() { ConcertzaalID = Convert.ToInt32(r["idconcertzaal"]), ConcertzaalNaam = r["concertzaalnaam"].ToString(), Locatie = r["locatie"].ToString(), Land = r["land"].ToString() };
                            dto.ConcertzaalID = Convert.ToInt32(r["idconcertzaal"]);
                            dto.Datum = Convert.ToDateTime(r["datum"]);
                            dto.BeginTijd = r["tijdbegin"].ToString();
                            dto.EindTijd = r["tijdeinde"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            return dto;
        }


        public void AddConcert(ConcertDTO concert)
        {

            query = "INSERT INTO concerten(idband, idconcertzaal, datum, tijdbegin, tijdeinde) VALUES(@idband, @idconcertzaal, @datum, @tijdbegin, @tijdeinde)";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@idband", concert.BandID);
                        q.Parameters.AddWithValue("@idconcertzaal", concert.ConcertzaalID);
                        q.Parameters.AddWithValue("@datum", concert.Datum);
                        q.Parameters.AddWithValue("@tijdbegin", concert.BeginTijd);
                        q.Parameters.AddWithValue("@tijdeinde", concert.EindTijd);
                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
        public void DeleteConcert(ConcertDTO concert)
        {
            query = "DELETE FROM concerten WHERE id=@id";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@id", concert.ID);
                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }

        public List<ConcertDTO> GetAll()
        {
            List<ConcertDTO> dtoC = new List<ConcertDTO>();


            query =
                @"SELECT concerten.id
                        , bands.idband
                        , bands.bandnaam
                        , bands.infoband
                        , concertzaal.idconcertzaal
                        , concertzaal.concertzaalnaam
                        , concertzaal.locatie
                        , concertzaal.land
                        , concerten.datum
                        , concerten.tijdbegin
                        , concerten.tijdeinde 
                    FROM concerten 
                    JOIN bands 
                      ON bands.idband=concerten.idband 
                    JOIN concertzaal 
                      ON concertzaal.idconcertzaal=concerten.idconcertzaal
                ORDER BY concerten.datum";
            /*query = "SELECT * FROM concerten";*/
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        SqlDataReader r = q.ExecuteReader();

                        while (r.Read())
                        {
                            ConcertDTO dto = new ConcertDTO();


                            dto.ID = Convert.ToInt32(r["id"]);
                            dto.BandID = Convert.ToInt32(r["idband"]);
                            dto.BandDTO = new BandDTO() { BandID = Convert.ToInt32(r["idband"]), BandNaam = r["bandnaam"].ToString(), BandInfo = r["infoband"].ToString() };
                            dto.ConcertzaalDTO = new ConcertzaalDTO() { ConcertzaalID = Convert.ToInt32(r["idconcertzaal"]), ConcertzaalNaam = r["concertzaalnaam"].ToString(), Locatie = r["locatie"].ToString(), Land = r["land"].ToString() };
                            dto.ConcertzaalID = Convert.ToInt32(r["idconcertzaal"]);
                            dto.Datum = Convert.ToDateTime(r["datum"]);
                            dto.BeginTijd = r["tijdbegin"].ToString();
                            dto.EindTijd = r["tijdeinde"].ToString();

                            dtoC.Add(dto);
                        }

                        r.Close();
                        connection.Close();

                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            return dtoC;
            Console.WriteLine(dtoC);
        }
        public List<ConcertDTO> GetConcertsByBand(int id)
        {
           
            query = @"SELECT * FROM concerten  
                        JOIN bands 
                          ON bands.idband=concerten.idband
                        JOIN concertzaal 
                          ON concertzaal.idconcertzaal=concerten.idconcertzaal
                       WHERE concerten.idband=@idband";

            List<ConcertDTO> dtoB = new List<ConcertDTO>();
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@idband", id);
                        q.ExecuteNonQuery();
                        SqlDataReader r = q.ExecuteReader();

                        while (r.Read())
                        {
                            ConcertDTO dto = new ConcertDTO();


                            dto.ID = Convert.ToInt32(r["id"]);
                            dto.BandID = Convert.ToInt32(r["idband"]);
                            dto.BandDTO = new BandDTO() { BandID = Convert.ToInt32(r["idband"]), BandNaam = r["bandnaam"].ToString(), BandInfo = r["infoband"].ToString() };
                            dto.ConcertzaalDTO = new ConcertzaalDTO() { ConcertzaalID = Convert.ToInt32(r["idconcertzaal"]), ConcertzaalNaam = r["concertzaalnaam"].ToString(), Locatie = r["locatie"].ToString(), Land = r["land"].ToString() };
                            dto.ConcertzaalID = Convert.ToInt32(r["idconcertzaal"]);
                            dto.Datum = Convert.ToDateTime(r["datum"]);
                            dto.BeginTijd = r["tijdbegin"].ToString();
                            dto.EindTijd = r["tijdeinde"].ToString();

                            dtoB.Add(dto);
                        }

                        r.Close();
                        connection.Close();

                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            return dtoB;
            Console.WriteLine(dtoB);
        }

        public List<ConcertDTO> GetConcertsByConcertzaal(int id)
        {

            query = @"SELECT * FROM concerten  
                        JOIN bands 
                          ON bands.idband=concerten.idband
                        JOIN concertzaal 
                          ON concertzaal.idconcertzaal=concerten.idconcertzaal
                       WHERE concerten.idconcertzaal=@idconcertzaal";

            List<ConcertDTO> dtoB = new List<ConcertDTO>();
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@idconcertzaal", id);
                        q.ExecuteNonQuery();
                        SqlDataReader r = q.ExecuteReader();

                        while (r.Read())
                        {
                            ConcertDTO dto = new ConcertDTO();


                            dto.ID = Convert.ToInt32(r["id"]);
                            dto.BandID = Convert.ToInt32(r["idband"]);
                            dto.BandDTO = new BandDTO() { BandID = Convert.ToInt32(r["idband"]), BandNaam = r["bandnaam"].ToString(), BandInfo = r["infoband"].ToString() };
                            dto.ConcertzaalDTO = new ConcertzaalDTO() { ConcertzaalID = Convert.ToInt32(r["idconcertzaal"]), ConcertzaalNaam = r["concertzaalnaam"].ToString(), Locatie = r["locatie"].ToString(), Land = r["land"].ToString() };
                            dto.ConcertzaalID = Convert.ToInt32(r["idconcertzaal"]);
                            dto.Datum = Convert.ToDateTime(r["datum"]);
                            dto.BeginTijd = r["tijdbegin"].ToString();
                            dto.EindTijd = r["tijdeinde"].ToString();

                            dtoB.Add(dto);
                        }

                        r.Close();
                        connection.Close();

                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            return dtoB;
            Console.WriteLine(dtoB);
        }
    }
}



