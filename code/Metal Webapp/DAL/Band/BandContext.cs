﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DAL
{
    public class BandContext : IBandContext
    {
        //private string conn = "Data Source=DESKTOP-HOADRGG;Initial Catalog=Fontys metal;Integrated Security=True"; // - DESKTOP LOKALE DATABASE
        private string conn = "Data Source=LAPTOP-4LVGLOHE;Initial Catalog=Fontys metal;Integrated Security=True"; // - LAPTOP LOKALE DATABASE
        private string query;

        public void UpdateBand(BandDTO band)
        {
            query = "UPDATE bands SET bandnaam = @bandnaam, infoband = @infoband WHERE idband = @idband";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@idband", band.BandID);
                        q.Parameters.AddWithValue("@bandnaam", band.BandNaam);
                        q.Parameters.AddWithValue("@infoband", band.BandInfo);
                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
        public void AddBand(BandDTO band)
        {
            query = "INSERT INTO bands(bandnaam, infoband) VALUES(@bandnaam, @infoband)";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@bandnaam", band.BandNaam);
                        q.Parameters.AddWithValue("@infoband", band.BandInfo);
                        q.ExecuteNonQuery();
                    }
                }
            } 
            catch (Exception e)
            {
                Console.Write(e);
            }
      
        }
        public void DeleteBand(BandDTO band)
        {
            query = "DELETE FROM bands WHERE idband=@idband";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@idband", band.BandID);
                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }

        public BandDTO GetByID(int id)
        {
            query = "SELECT * FROM bands WHERE idband=@idband";

            BandDTO dto = new BandDTO();
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@idband", id);

                        SqlDataReader r = q.ExecuteReader();

                        while (r.Read())
                        {
                            dto.BandID = Convert.ToInt32(r["idband"]);
                            dto.BandNaam = r["bandnaam"].ToString();
                            dto.BandInfo = r["infoband"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            return dto;
        }
        public List<BandDTO> GetAll()
        {
            List<BandDTO> dtoB = new List<BandDTO>();


            /*query = "SELECT concerten.id, bands.bandnaam, concertzaal.concertzaalnaam, concerten.datum, concerten.tijdbegin, concerten.tijdeinde FROM concerten JOIN bands ON bands.idband=concerten.idband JOIN concertzaal ON concertzaal.idconcertzaal=concerten.idconcertzaal";*/
            query = "SELECT * FROM bands ORDER BY bandnaam";
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        
                        q.ExecuteNonQuery();
                        SqlDataReader r = q.ExecuteReader();

                        while (r.Read())
                        {
                            BandDTO dto = new BandDTO();
                            dto.BandID = Convert.ToInt32(r["idband"]);
                            dto.BandNaam = r["bandnaam"].ToString();
                            dto.BandInfo = r["infoband"].ToString();
                            dtoB.Add(dto);
                        }

                        r.Close();
                        connection.Close();

                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            return dtoB;
            Console.WriteLine(dtoB);
        }

       
    }
}
