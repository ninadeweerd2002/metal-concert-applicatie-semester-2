﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IBandContext
    {
        public void AddBand(BandDTO band);
        public void DeleteBand(BandDTO band);
        public void UpdateBand(BandDTO band);
        public BandDTO GetByID(int id);
        public List<BandDTO> GetAll();
    }
}
