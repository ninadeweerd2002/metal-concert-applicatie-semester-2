﻿using System.ComponentModel.DataAnnotations;


namespace DAL
{
    public class BandDTO
    {
        public int BandID { get; set; }
        public string BandNaam { get; set; }
        public string BandInfo { get; set; }
        
        public BandDTO(int bandid, string bandnaam, string bandinfo)
        {
            BandID = bandid;
            BandNaam = bandnaam;
            BandInfo = bandinfo;
        }
        public BandDTO(int bandid)
        {
            BandID = bandid;
            
        }

        public BandDTO()
        {
        }
    }
}