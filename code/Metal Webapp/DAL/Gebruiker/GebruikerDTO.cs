﻿using System.ComponentModel.DataAnnotations;

namespace DAL
{
    public class GebruikerDTO
    {
        public int ID { get; set; }
        public string Gebruikersnaam { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int GebruikerID { get; set; }
        public int BandID { get; set; }
        public BandDTO BandDTO { get; set; }

        public GebruikerDTO(int id, int gebruikerid, BandDTO bandnaam, string gb, string email, string pw, int fanBandID)
        {
            ID = id;
            BandDTO = bandnaam;
            Gebruikersnaam = gb;
            Email = email;
            Password = pw;
            BandID = fanBandID;
            GebruikerID = gebruikerid;
        }
        public GebruikerDTO(int id, string gb, string email, string pw, int fanBandID)
        {
            ID = id;
            Gebruikersnaam = gb;
            Email = email;
            Password = pw;
            BandID = fanBandID;
        }
        public GebruikerDTO(string gb, string pw)
        {          
            Gebruikersnaam = gb;
            Password = pw;
        }

        public GebruikerDTO(int id)
        {
            ID = id;
        }
        public GebruikerDTO()
        {
        }
        public GebruikerDTO(int id, int gebruikerid,int bandid)
        {
            ID = id;
            GebruikerID = gebruikerid;
            BandID = bandid;
        }
        public GebruikerDTO(int id, string gebruikersnaam, string password)
        {
            ID = id;
            Gebruikersnaam = gebruikersnaam;
            Password = password;
        }

       
    }
}