﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DAL
{
    public class GebruikerContext : IGebruikerContext
    {
        private string conn = "Data Source=LAPTOP-4LVGLOHE;Initial Catalog=Fontys metal;Integrated Security=True"; // - LAPTOP LOKALE DATABASE
        //private string conn = "Data Source=DESKTOP-HOADRGG;Initial Catalog=Fontys metal;Integrated Security=True"; // - DESKTOP LOKALE DATABASE
        private string query;
        public void AddGebruiker(GebruikerDTO gebruiker)
        {

            query = "INSERT INTO gebruikers (username, email, password, idband) VALUES (@username, @email, @password, @idband)";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    { 
                        q.Parameters.AddWithValue("@username", gebruiker.Gebruikersnaam);
                        q.Parameters.AddWithValue("@email", gebruiker.Email);
                        q.Parameters.AddWithValue("@password", gebruiker.Password);
                        q.Parameters.AddWithValue("@idband", gebruiker.BandID);

                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }

        public int Inloggen(GebruikerDTO gebruiker)
        {
            query = @"SELECT * FROM gebruikers
                        JOIN bands 
                          ON bands.idband=gebruikers.idband
                       WHERE username=@username AND password=@password";

            bool login = false;
            int id = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {

                      
                        q.Parameters.AddWithValue("@username", gebruiker.Gebruikersnaam);
                        q.Parameters.AddWithValue("@password", gebruiker.Password);
                        

                        if(q.ExecuteScalar() != null)
                        {
                            id = (int)q.ExecuteScalar();
                            login = true;

                        }

                        connection.Close();
                        
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                
            }
            return id;
        }
        public void UpdateGebruiker(GebruikerDTO gebruiker)
        {
            query = "UPDATE gebruikers SET username = @gebruikersnaam, email = @email, password = @password WHERE id = @id";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@id", gebruiker.ID);
                        q.Parameters.AddWithValue("@gebruikersnaam", gebruiker.Gebruikersnaam);
                        q.Parameters.AddWithValue("@email", gebruiker.Email);
                        q.Parameters.AddWithValue("@password", gebruiker.Password);
                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }

        public void DeleteGebruiker(GebruikerDTO gebruiker)
        {
            query = "DELETE FROM gebruikers WHERE id=@id";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@id", gebruiker.ID);
                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
        public List<GebruikerDTO> BandLibrary(int id)
        {
            List<GebruikerDTO> dtoCz = new List<GebruikerDTO>();

            query = @"SELECT * FROM koppeltable
                        JOIN bands 
                          ON bands.idband=koppeltable.idband
                       WHERE idgebruiker = @idgebruiker";
           
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@idgebruiker", id);
                        q.ExecuteNonQuery();
                        SqlDataReader r = q.ExecuteReader();

                        while (r.Read())
                        {
                            GebruikerDTO dto = new GebruikerDTO();


                            dto.ID = Convert.ToInt32(r["id"]);
                            dto.GebruikerID = Convert.ToInt32(r["idgebruiker"]);
                            dto.BandID = Convert.ToInt32(r["idband"]);
                            dto.BandDTO = new BandDTO() { BandID = Convert.ToInt32(r["idband"]), BandNaam = r["bandnaam"].ToString(), BandInfo = r["infoband"].ToString() };

                            dtoCz.Add(dto);
                        }

                        r.Close();
                        connection.Close();

                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            return dtoCz;
            Console.WriteLine(dtoCz);
        }

        public List<GebruikerDTO> GetAll()
        {
            List<GebruikerDTO> dtoCz = new List<GebruikerDTO>();

            query = @"SELECT * FROM gebruikers
                        JOIN bands 
                          ON bands.idband=gebruikers.idband";
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.ExecuteNonQuery();
                        SqlDataReader r = q.ExecuteReader();

                        while (r.Read())
                        {
                            GebruikerDTO dto = new GebruikerDTO();
                            dto.ID = Convert.ToInt32(r["id"]);
                            dto.Gebruikersnaam = r["username"].ToString();
                            dto.Email = r["email"].ToString();
                            dto.Password = r["password"].ToString();
                            dto.BandID = Convert.ToInt32(r["idband"]);
                            dto.BandDTO = new BandDTO() { BandID = Convert.ToInt32(r["idband"]), BandNaam = r["bandnaam"].ToString(), BandInfo = r["infoband"].ToString() };
                            dtoCz.Add(dto);
                        }

                        r.Close();
                        connection.Close();

                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            return dtoCz;
            Console.WriteLine(dtoCz);
        }

        
        public GebruikerDTO GetByID(int id)
        {
            query = @"SELECT * FROM gebruikers
                        JOIN bands 
                          ON bands.idband=gebruikers.idband
                        WHERE id=@id";

            GebruikerDTO dto = new GebruikerDTO();
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@id", id);

                        SqlDataReader r = q.ExecuteReader();

                        while (r.Read())
                        {
                            dto.ID = Convert.ToInt32(r["id"]);
                            dto.Gebruikersnaam = r["username"].ToString();
                            dto.Email = r["email"].ToString();
                            dto.Password = r["password"].ToString();
                            dto.BandID = Convert.ToInt32(r["idband"]);
                            dto.BandDTO = new BandDTO() { BandID = Convert.ToInt32(r["idband"]), BandNaam = r["bandnaam"].ToString(), BandInfo = r["infoband"].ToString() };
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            return dto;
        }
        public void AddBandToLibrary(GebruikerDTO gebruiker)
        {
            query = "INSERT INTO koppeltable (idgebruiker, idband) VALUES (@idgebruiker, @idband)";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@idgebruiker", gebruiker.GebruikerID);
                        q.Parameters.AddWithValue("@idband", gebruiker.BandID);

                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
        public void DeleteBandFromLibrary(GebruikerDTO gebruiker)
        {
            query = "DELETE FROM koppeltable WHERE id=@id";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@id", gebruiker.ID);
                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }

        public GebruikerDTO GetByIDKoppeltable(int id)
        {
            query = @"SELECT * FROM koppeltable
                        JOIN bands 
                          ON bands.idband=koppeltable.idband
                        WHERE id=@id";

            GebruikerDTO dto = new GebruikerDTO();
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@id", id);

                        SqlDataReader r = q.ExecuteReader();

                        while (r.Read())
                        {
                            dto.ID = Convert.ToInt32(r["id"]);
                            dto.GebruikerID = Convert.ToInt32(r["idgebruiker"]);
                            dto.BandID = Convert.ToInt32(r["idband"]);
                            dto.BandDTO = new BandDTO() { BandID = Convert.ToInt32(r["idband"]), BandNaam = r["bandnaam"].ToString(), BandInfo = r["infoband"].ToString() };
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            return dto;
        }
    }
}

