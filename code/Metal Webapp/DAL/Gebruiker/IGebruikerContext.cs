﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IGebruikerContext
    {
        public void AddGebruiker(GebruikerDTO gebruiker);
        public void UpdateGebruiker(GebruikerDTO gebruiker);
        public void DeleteGebruiker(GebruikerDTO gebruiker);
        public int Inloggen(GebruikerDTO gebruiker);
        public void AddBandToLibrary(GebruikerDTO gebruiker);
        public void DeleteBandFromLibrary(GebruikerDTO gebruiker);
        public List<GebruikerDTO> BandLibrary(int id);
        public List<GebruikerDTO> GetAll();
        public GebruikerDTO GetByID(int id);
        public GebruikerDTO GetByIDKoppeltable(int id);
    }
}
