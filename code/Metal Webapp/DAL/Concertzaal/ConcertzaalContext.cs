﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DAL
{
    public class ConcertzaalContext : IConcertzaalContext
    {
        //private string conn = "Data Source=DESKTOP-HOADRGG;Initial Catalog=Fontys metal;Integrated Security=True"; // - DESKTOP LOKALE DATABASE
        private string conn = "Data Source=LAPTOP-4LVGLOHE;Initial Catalog=Fontys metal;Integrated Security=True"; // - LAPTOP LOKALE DATABASE
        private string query;

        public void UpdateConcertzaal(ConcertzaalDTO concertzaal)
        {
            query = "UPDATE concertzaal SET concertzaalnaam = @concertzaalnaam, locatie = @locatie, land = @land WHERE idconcertzaal = @idconcertzaal";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@idconcertzaal", concertzaal.ConcertzaalID);
                        q.Parameters.AddWithValue("@concertzaalnaam", concertzaal.ConcertzaalNaam);
                        q.Parameters.AddWithValue("@locatie", concertzaal.Locatie);
                        q.Parameters.AddWithValue("@land", concertzaal.Land);
                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
        public void AddConcertzaal(ConcertzaalDTO concertzaal)
        {
            query = "INSERT INTO concertzaal(concertzaalnaam, locatie, land) VALUES(@concertzaalnaam, @locatie, @land)";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@concertzaalnaam", concertzaal.ConcertzaalNaam);
                        q.Parameters.AddWithValue("@locatie", concertzaal.Locatie);
                        q.Parameters.AddWithValue("@land", concertzaal.Land);
                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
        public void DeleteConcertzaal(ConcertzaalDTO concertzaal)
        {
            query = "DELETE FROM concertzaal WHERE idconcertzaal=@idconcertzaal";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@idconcertzaal", concertzaal.ConcertzaalID);
                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
        public List<ConcertzaalDTO> GetAll()
        {
            List<ConcertzaalDTO> dtoCz = new List<ConcertzaalDTO>();

            query = "SELECT * FROM concertzaal";
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.ExecuteNonQuery();
                        SqlDataReader r = q.ExecuteReader();

                        while (r.Read())
                        {
                            ConcertzaalDTO dto = new ConcertzaalDTO();
                            dto.ConcertzaalID = Convert.ToInt32(r["idconcertzaal"]);
                            dto.ConcertzaalNaam = r["concertzaalnaam"].ToString();
                            dto.Locatie = r["locatie"].ToString();
                            dto.Land = r["land"].ToString();
                            dtoCz.Add(dto);
                        }

                        r.Close();
                        connection.Close();

                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            return dtoCz;
            Console.WriteLine(dtoCz);
        }
        public ConcertzaalDTO GetByID(int id)
        {
            query = "SELECT * FROM concertzaal WHERE idconcertzaal=@idconcertzaal";

            ConcertzaalDTO dto = new ConcertzaalDTO();
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@idconcertzaal", id);

                        SqlDataReader r = q.ExecuteReader();

                        while (r.Read())
                        {
                            dto.ConcertzaalID = Convert.ToInt32(r["idconcertzaal"]);
                            dto.ConcertzaalNaam = r["concertzaalnaam"].ToString();
                            dto.Locatie = r["locatie"].ToString();
                            dto.Land = r["land"].ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            return dto;
        }
    }
}
