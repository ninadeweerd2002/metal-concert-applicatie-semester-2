﻿using System.ComponentModel.DataAnnotations;

namespace DAL
{
    public class ConcertzaalDTO
    {
        public int ConcertzaalID { get; set; }
        public string ConcertzaalNaam { get; set; }
        public string Locatie { get; set; }
        public string Land { get; set; }

        public ConcertzaalDTO(int concertzaalid, string concertzaalnaam, string locatie, string land)
        {
            ConcertzaalID = concertzaalid;
            ConcertzaalNaam = concertzaalnaam;
            Locatie = locatie;
            Land = land;
        }

        public ConcertzaalDTO()
        {
        }
        public ConcertzaalDTO(int concertzaalid)
        {
            ConcertzaalID = concertzaalid;
        }
    }
}