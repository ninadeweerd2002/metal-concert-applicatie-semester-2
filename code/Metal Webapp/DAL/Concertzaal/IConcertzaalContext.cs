﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IConcertzaalContext
    {
        public void AddConcertzaal(ConcertzaalDTO concertzaal);
        public void DeleteConcertzaal(ConcertzaalDTO concertzaal);
        public void UpdateConcertzaal(ConcertzaalDTO concertzaal);
        public List<ConcertzaalDTO> GetAll();
        public ConcertzaalDTO GetByID(int id);
    }
}
