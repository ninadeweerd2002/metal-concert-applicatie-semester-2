﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BLL;

namespace UnitTests
{
    [TestClass]
    public class ConcertzaalTest
    {
        private ConcertzaalStub stub = new ConcertzaalStub();
        private List<ConcertzaalModel> concertzalen = new List<ConcertzaalModel>();


        [TestMethod]
        public void ConcertzaalGetAllTest()
        {
            ConcertzaalContainer concertCon = new ConcertzaalContainer(stub);
            concertzalen = concertCon.GetAll();

            Assert.IsNotNull(concertzalen);
            Assert.AreEqual(concertzalen.Count, 4);
        }

        [TestMethod]
        public void AddConcertenzaalTest()
        {
            ConcertzaalContainer concertCon = new ConcertzaalContainer(stub);
            ConcertzaalModel concert = new ConcertzaalModel()
            {
                
                ConcertzaalID = 4,
                ConcertzaalNaam = "013 Poppodium",
                Locatie = "Tilburg",
                Land = "Nederland",
               

            };
            concertCon.AddConcertzaal(concert);

            Assert.AreEqual(stub.Concertzaal[stub.Concertzaal.Count - 1].ConcertzaalID, concert.ConcertzaalID);
            Assert.AreEqual(stub.Concertzaal[stub.Concertzaal.Count - 1].ConcertzaalNaam, concert.ConcertzaalNaam);
            Assert.AreEqual(stub.Concertzaal[stub.Concertzaal.Count - 1].Locatie, concert.Locatie);
            Assert.AreEqual(stub.Concertzaal[stub.Concertzaal.Count - 1].Land, concert.Land);
        }
        [TestMethod]
        public void GetConcertByIDTest()
        {
            ConcertzaalContainer concertCon = new ConcertzaalContainer(stub);
            ConcertzaalModel concert = concertCon.GetByID(3);

            Assert.IsNotNull(concert);
            Assert.AreEqual("Trix", concert.ConcertzaalNaam);
            Assert.AreEqual("Trix", stub.Concertzaal[1].ConcertzaalNaam);
            Assert.AreEqual(stub.Concertzaal[1].ConcertzaalID, concert.ConcertzaalID);
        }
        [TestMethod]
        public void DeleteConcertTest()
        {
            ConcertzaalContainer concertCon = new ConcertzaalContainer(stub);

            ConcertzaalModel concertzaal = new ConcertzaalModel() { ConcertzaalID = 3, ConcertzaalNaam = "Trix", Locatie = "Antwerpen", Land = "Belgie" };
            Assert.AreEqual(concertzaal.ConcertzaalID, stub.Concertzaal[1].ConcertzaalID);
            concertCon.DeleteConcertzaal(concertzaal);
            Assert.AreNotEqual(concertzaal.ConcertzaalID, stub.Concertzaal[1].ConcertzaalID);
        }

        [TestMethod]
        public void UpdateConcertTest()
        {
            ConcertzaalContainer concertCon = new ConcertzaalContainer(stub);

            ConcertzaalModel concert = new ConcertzaalModel()
            {
               
                ConcertzaalID = 1,
                ConcertzaalNaam = "De Warande",
                Locatie = "Turnhout",
                Land = "Belgie",
                
            };
            concertCon.UpdateConcertzaal(concert);

            Assert.IsNotNull(concert);
            
            Assert.AreEqual(stub.Concertzaal[0].ConcertzaalID, concert.ConcertzaalID);
            Assert.AreEqual(stub.Concertzaal[0].ConcertzaalNaam, concert.ConcertzaalNaam);
            Assert.AreEqual(stub.Concertzaal[0].Locatie, concert.Locatie);
            Assert.AreEqual(stub.Concertzaal[0].Land, concert.Land);

        }
    }
}
