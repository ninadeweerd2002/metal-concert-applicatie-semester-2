﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL;
using DAL;
using Metal_Webapp;

namespace UnitTests
{
    [TestClass]
    public class BandTest
    {
        private BandStub stub = new BandStub();
        private List<BandModel> bands = new List<BandModel>();

        [TestMethod]
        public void BandsTestGetAll()
        {
            BandContainer bandCon = new BandContainer(stub);
            bands = bandCon.GetAll();

            Assert.IsNotNull(bands);
            Assert.AreEqual(bands.Count, 5);
        }
        [TestMethod]
        public void AddBandTest()
        {
            BandContainer bandCon = new BandContainer(stub);
            BandModel band = new BandModel()
            {
                
                BandID = 4,
                BandNaam = "Royal Republic",
                BandInfo = "This is Royal Republic...."

            };
            bandCon.AddBand(band);

            Assert.AreEqual(stub.Band[stub.Band.Count - 1].BandID, band.BandID);
            Assert.AreEqual(stub.Band[stub.Band.Count - 1].BandNaam, band.BandNaam);
            Assert.AreEqual(stub.Band[stub.Band.Count - 1].BandInfo, band.BandInfo);
        }

        [TestMethod]
        public void GetBandByIDTest()
        {
            BandContainer bandCon = new BandContainer(stub);
            BandModel band = bandCon.GetByID(4);

            Assert.IsNotNull(band);
            Assert.AreEqual(4, band.BandID);
            Assert.AreEqual("Sabaton", band.BandNaam);
            Assert.AreEqual("Sabaton", stub.Band[2].BandNaam);
            Assert.AreEqual(stub.Band[2].BandID, band.BandID);
            Assert.AreEqual(stub.Band[2].BandInfo, band.BandInfo);
        }

        [TestMethod]
        public void DeleteBandTest()
        {
            BandContainer bandCon = new BandContainer(stub);

            BandModel band = new BandModel() { BandID = 3, BandNaam = "Amaranthe", BandInfo = "This is Amaranthe...." };
            Assert.AreEqual(band.BandID, stub.Band[0].BandID);
            bandCon.DeleteBand(band);
            Assert.AreNotEqual(band.BandID, stub.Band[0].BandID);
        }

        [TestMethod]
        public void UpdateBandTest()
        {
            BandContainer bandCon = new BandContainer(stub);

            BandModel band = new BandModel()
            {

                BandID = 3,
                BandNaam = "Alestorm",
                BandInfo = "This is Alestorm.... not Rammstein"

            };
            bandCon.UpdateBand(band);

            Assert.IsNotNull(band);
            Assert.AreEqual(stub.Band[0].BandID, band.BandID);
            Assert.AreEqual("Alestorm", stub.Band[0].BandNaam);
            Assert.AreEqual(stub.Band[0].BandNaam, band.BandNaam);
            Assert.AreEqual(stub.Band[0].BandInfo, band.BandInfo);
        }
    }
}
