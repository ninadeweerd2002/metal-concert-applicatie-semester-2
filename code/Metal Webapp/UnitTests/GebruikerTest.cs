﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL;

namespace UnitTests
{
    [TestClass]
    public class GebruikerTest
    {
        private GebruikerStub stub = new GebruikerStub();
        private List<GebruikerModel> gebruikers = new List<GebruikerModel>();

        [TestMethod]
        public void GebruikerTestGetAll()
        {
            GebruikerContainer gebruikerCon = new GebruikerContainer(stub);
            gebruikers = gebruikerCon.GetAll();

            Assert.IsNotNull(gebruikers);
            Assert.AreEqual(gebruikers.Count, 3);
        }
        [TestMethod]
        public void RegisterenTest()
        {
            GebruikerContainer gebruikerCon = new GebruikerContainer(stub);
            GebruikerModel gebruiker = new GebruikerModel()
            {

                ID = 4,
                Gebruikersnaam = "test",
                Email = "test@gmail.com",
                Password = "test"

            };
            gebruikerCon.AddGebruiker(gebruiker);

            Assert.AreEqual(stub.Gebruiker[stub.Gebruiker.Count - 1].ID, gebruiker.ID);
            Assert.AreEqual(stub.Gebruiker[stub.Gebruiker.Count - 1].Gebruikersnaam, gebruiker.Gebruikersnaam);
            Assert.AreEqual(stub.Gebruiker[stub.Gebruiker.Count - 1].Email, gebruiker.Email);
            Assert.AreEqual(stub.Gebruiker[stub.Gebruiker.Count - 1].Password, gebruiker.Password);
        }

        [TestMethod]
        public void GetByIDTest()
        {
            GebruikerContainer gebruikerCon = new GebruikerContainer(stub);
            GebruikerModel gebruiker = gebruikerCon.GetByID(7);

            Assert.IsNotNull(gebruiker);
            Assert.AreEqual("zino", gebruiker.Gebruikersnaam);
            Assert.AreEqual(stub.Gebruiker[1].ID, gebruiker.ID);
        }

        [TestMethod]
        public void DeleteGebruikerTest()
        {
            GebruikerContainer gebruikerCon = new GebruikerContainer(stub);
            GebruikerModel gebruiker = new GebruikerModel() { ID = 2, Gebruikersnaam = "nina", Email = "ninatest@gmail.com", Password = "nina" };

            Assert.AreEqual(gebruiker.ID, stub.Gebruiker[0].ID);
            gebruikerCon.DeleteGebruiker(gebruiker);
            Assert.AreNotEqual(gebruiker.ID, stub.Gebruiker[0].ID);
        }

        [TestMethod]
        public void UpdateGebruikerTest()
        {
            GebruikerContainer gebruikerCon = new GebruikerContainer(stub);

            GebruikerModel gebruiker = new GebruikerModel()
            {

                ID = 2,
                Gebruikersnaam = "nino",
                Email = "ninotest@gmail.com",
                Password = "nino"

            };
            gebruikerCon.UpdateGebruiker(gebruiker);

            Assert.IsNotNull(gebruiker);
            Assert.AreEqual(stub.Gebruiker[0].ID, gebruiker.ID);
            Assert.AreEqual(stub.Gebruiker[0].Gebruikersnaam, gebruiker.Gebruikersnaam);
            Assert.AreEqual(stub.Gebruiker[0].Email, gebruiker.Email);
            Assert.AreEqual(stub.Gebruiker[0].Password, gebruiker.Password);
        }
        [TestMethod]
        public void InloggenTest()
        {
            GebruikerContainer gebruikerCon = new GebruikerContainer(stub);

            GebruikerModel gebruiker = new GebruikerModel()
            {

                Gebruikersnaam = "nina",
                Password = "nina"

            };
            gebruikerCon.Inloggen(gebruiker);

            Assert.IsNotNull(gebruiker);
            Assert.AreEqual(stub.Gebruiker[0].Gebruikersnaam, gebruiker.Gebruikersnaam);
            Assert.AreEqual(stub.Gebruiker[0].Password, gebruiker.Password);
        }

        [TestMethod]
        public void BandLibraryTest()
        {
            GebruikerContainer gebruikerCon = new GebruikerContainer(stub);

            List<GebruikerModel> bands = gebruikerCon.BandLibrary(7);

            Assert.IsNotNull(bands);
            Assert.AreEqual(bands.Count, 2);
        }

        [TestMethod]
        public void AddBandToLibraryTest()
        {
            GebruikerContainer gebruikerCon = new GebruikerContainer(stub);
            GebruikerModel gebruiker = new GebruikerModel()
            {

                ID = 11,
                GebruikerID = 2,
                BandID = 3,
                BandNaam = "Alestorm"

            };
            gebruikerCon.AddBandToLibrary(gebruiker);

            Assert.AreEqual(stub.BandLib[stub.BandLib.Count - 1].ID, gebruiker.ID);
            Assert.AreEqual(stub.BandLib[stub.BandLib.Count - 1].GebruikerID, gebruiker.GebruikerID);
            Assert.AreEqual(stub.BandLib[stub.BandLib.Count - 1].BandID, gebruiker.BandID);
        }

        [TestMethod]
        public void GetByIDKoppeltableTest()
        {
            GebruikerContainer gebruikerCon = new GebruikerContainer(stub);
            GebruikerModel gebruiker = gebruikerCon.GetByIDKoppeltable(4);

            Assert.IsNotNull(gebruiker);
            Assert.AreEqual(7, gebruiker.GebruikerID);
            Assert.AreEqual(stub.BandLib[2].ID, gebruiker.ID);
        }

        [TestMethod]
        public void DeleteBandFromLibraryTest()
        {
            GebruikerContainer gebruikerCon = new GebruikerContainer(stub);
            GebruikerModel gebruiker = new GebruikerModel() { ID = 6, GebruikerID = 2, BandID = 1};

            Assert.AreEqual(gebruiker.ID, stub.BandLib[3].ID);
            gebruikerCon.DeleteBandFromLibrary(gebruiker);
            Assert.AreNotEqual(gebruiker.ID, stub.BandLib[3].ID);
        }
    }
}
