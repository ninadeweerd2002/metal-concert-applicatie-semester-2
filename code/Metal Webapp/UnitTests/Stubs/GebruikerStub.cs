﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace UnitTests
{
    public class GebruikerStub : IGebruikerContext
    {
        public List<GebruikerDTO> Gebruiker { get; set; }
        public List<GebruikerDTO> BandLib { get; set; }

        public GebruikerStub()
        {
            Gebruiker = new List<GebruikerDTO>()
            {
                new GebruikerDTO() {ID = 2, Gebruikersnaam = "nina", Email = "ninatest@gmail.com", Password = "nina", BandID = 1, BandDTO = new BandDTO{ BandNaam = "Rammstein"}},
                new GebruikerDTO() {ID = 7, Gebruikersnaam = "zino", Email = "zinotest@gmail.com", Password = "zino", BandID = 7, BandDTO = new BandDTO{ BandNaam = "Electric Callboy"}},
                new GebruikerDTO() {ID = 10, Gebruikersnaam = "admin", Email = "admin@gmail.com", Password = "admin", BandID = 9, BandDTO = new BandDTO { BandNaam = "Royal Republic" }}
            };

            BandLib = new List<GebruikerDTO>()
            {
                new GebruikerDTO() {ID = 1, GebruikerID = 2, BandID = 1, BandDTO = new BandDTO{ BandNaam = "Rammstein"}},
                new GebruikerDTO() {ID = 3, GebruikerID = 7, BandID = 7, BandDTO = new BandDTO{ BandNaam = "Electric Callboy"}},
                new GebruikerDTO() {ID = 4, GebruikerID = 7, BandID = 4, BandDTO = new BandDTO{ BandNaam = "Alestrom"}},
                new GebruikerDTO() {ID = 6, GebruikerID = 2, BandID = 6, BandDTO = new BandDTO{ BandNaam = "Distrubed"}},
                new GebruikerDTO() {ID = 8, GebruikerID = 10, BandID = 1, BandDTO = new BandDTO{ BandNaam = "Royal Republic"}}
            };
        }

        public void AddGebruiker(GebruikerDTO gebruiker)
        {
            Gebruiker.Add(gebruiker);
        }
        public void UpdateGebruiker(GebruikerDTO gebruiker)
        {
            foreach (GebruikerDTO gebruikers in Gebruiker)
            {
                if (gebruikers.ID == gebruiker.ID)
                {
                    gebruikers.Gebruikersnaam = gebruiker.Gebruikersnaam;
                    gebruikers.Email = gebruiker.Email;
                    gebruikers.Password = gebruiker.Password;
                    
                }
            }
        }
        public void DeleteGebruiker(GebruikerDTO gebruikerdto)
        {
            GebruikerDTO dto = new GebruikerDTO();
            foreach (GebruikerDTO gebruiker in Gebruiker)
            {
                if (gebruikerdto.ID == gebruiker.ID)
                {
                    dto = gebruiker;
                }
            }

            Gebruiker.Remove(dto);
        }
        public int Inloggen(GebruikerDTO gebruiker)
        {
            foreach (GebruikerDTO gebruikers in Gebruiker)
            {
                if (gebruikers.Gebruikersnaam == gebruiker.Gebruikersnaam && gebruikers.Password == gebruiker.Password)
                {
                    return gebruikers.ID;
                }
            }
            return 0;
        }
        
        public GebruikerDTO GetByID(int ID)
        {
            foreach (GebruikerDTO gebruikers in Gebruiker)
            {
                if (gebruikers.ID == ID)
                {
                    return gebruikers;
                }
            }
            return new GebruikerDTO();
        }
        public List<GebruikerDTO> GetAll()
        {
            List<GebruikerDTO> gebruikers = new List<GebruikerDTO>();

            foreach (GebruikerDTO gebruiker in Gebruiker)
            {
                gebruikers.Add(gebruiker);
            }
            return Gebruiker;
        }

        public void AddBandToLibrary(GebruikerDTO gebruiker)
        {
            BandLib.Add(gebruiker);
        }

        public void DeleteBandFromLibrary(GebruikerDTO gebruikerdto)
        {
            GebruikerDTO dto = new GebruikerDTO();
            foreach (GebruikerDTO gebruiker in BandLib)
            {
                if (gebruikerdto.ID == gebruiker.ID)
                {
                    dto = gebruiker;
                }
            }

            BandLib.Remove(dto);
        }

        public List<GebruikerDTO> BandLibrary(int id)
        {
            List<GebruikerDTO> gebruiker = new List<GebruikerDTO>();
            foreach (GebruikerDTO item in BandLib)
            {
                if (item.GebruikerID == id)
                {
                    gebruiker.Add(item);
                }
            }
            return gebruiker;
        }

        public GebruikerDTO GetByIDKoppeltable(int id)
        {
            foreach (GebruikerDTO gebruikers in BandLib)
            {
                if (gebruikers.ID == id)
                {
                    return gebruikers;
                }
            }
            return new GebruikerDTO();
        }
    }
}
