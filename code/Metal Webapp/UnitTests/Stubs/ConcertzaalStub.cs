﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace UnitTests
{
    public class ConcertzaalStub : IConcertzaalContext
    {
        public List<ConcertzaalDTO> Concertzaal { get; set; }

        public ConcertzaalStub()
        {
            Concertzaal = new List<ConcertzaalDTO>()
            {
                new ConcertzaalDTO() { ConcertzaalID = 1, ConcertzaalNaam = "013 Poppodium", Locatie = "Tilburg", Land = "Nederland"},
                new ConcertzaalDTO() { ConcertzaalID = 3, ConcertzaalNaam = "Trix", Locatie = "Antwerpen", Land = "Belgie"},
                new ConcertzaalDTO() { ConcertzaalID = 5, ConcertzaalNaam = "Het Depot", Locatie = "Leuven", Land = "Belgie"},
                new ConcertzaalDTO() { ConcertzaalID = 7, ConcertzaalNaam = "Ahoy", Locatie = "Rotterdam", Land = "Nederland"}
            };
        }

        public void AddConcertzaal(ConcertzaalDTO concertzaal)
        {
            Concertzaal.Add(concertzaal);
        }
        public void DeleteConcertzaal(ConcertzaalDTO concertzaaldto)
        {
            ConcertzaalDTO dto = new ConcertzaalDTO();
            foreach (ConcertzaalDTO concertzaal in Concertzaal)
            {
                if (concertzaaldto.ConcertzaalID == concertzaal.ConcertzaalID)
                {
                    dto = concertzaal;
                }
            }

            Concertzaal.Remove(dto);
        }
        public void UpdateConcertzaal(ConcertzaalDTO concertzaal)
        {
            foreach (ConcertzaalDTO concertzalen in Concertzaal)
            {
                if (concertzalen.ConcertzaalID == concertzaal.ConcertzaalID)
                {
                    concertzalen.ConcertzaalNaam = concertzaal.ConcertzaalNaam;
                    concertzalen.Locatie = concertzaal.Locatie;
                    concertzalen.Land = concertzaal.Land;

                }
            }
        }
        public ConcertzaalDTO GetByID(int ID)
        {
            foreach (ConcertzaalDTO concertzalen in Concertzaal)
            {
                if (concertzalen.ConcertzaalID == ID)
                {
                    return concertzalen;
                }
            }
            return new ConcertzaalDTO();
        }
        public List<ConcertzaalDTO> GetAll()
        {
            List<ConcertzaalDTO> concertzalen = new List<ConcertzaalDTO>();

            foreach (ConcertzaalDTO concertzaal in Concertzaal)
            {
                concertzalen.Add(concertzaal);
            }
            return Concertzaal;
        }
    }
}
