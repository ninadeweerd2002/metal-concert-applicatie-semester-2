﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace UnitTests
{
    public class BandStub : IBandContext
    {
        public List<BandDTO> Band { get; set; }

        public BandStub()
        {
            Band = new List<BandDTO>()
            {
                new BandDTO() { BandID = 3, BandNaam = "Amaranthe", BandInfo = "This is Amaranthe...." },
                new BandDTO() { BandID = 6, BandNaam = "Rammstein", BandInfo = "This is Rammstein...." },
                new BandDTO() { BandID = 4, BandNaam = "Sabaton", BandInfo = "This is Sabathon...." },
                new BandDTO() { BandID = 7, BandNaam = "Powerwolf", BandInfo = "This is Powerwolf...." },
                new BandDTO() { BandID = 9, BandNaam = "Korn", BandInfo = "This is Korn...." }
            };
        }
        public void AddBand(BandDTO band)
        {
            Band.Add(band);
        }
        public void DeleteBand(BandDTO banddto)
        {
            BandDTO dto = new BandDTO();
            foreach (BandDTO band in Band)
            {
                if (banddto.BandID == band.BandID)
                {
                    dto = band;
                }
            }

            Band.Remove(dto);
        }
        public void UpdateBand(BandDTO band)
        {
            foreach(BandDTO bands in Band)
            {
                if (bands.BandID == band.BandID)
                {
                    bands.BandNaam = band.BandNaam;
                    bands.BandInfo = band.BandInfo;
                }
            }
        }
        public BandDTO GetByID(int ID)
        {
            foreach (BandDTO bands in Band)
            {
                if (bands.BandID == ID)
                {
                    return bands;
                }
            }
            return new BandDTO();
        }
        public List<BandDTO> GetAll()
        {
            List<BandDTO> bands = new List<BandDTO>();

            foreach (BandDTO band in Band)
            {
                bands.Add(band);
            }
            return Band;
        }
    }
}
