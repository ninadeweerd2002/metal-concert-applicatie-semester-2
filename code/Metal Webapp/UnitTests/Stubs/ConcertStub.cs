﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BLL;

namespace UnitTests
{
    
    public class ConcertStub : IConcertContext
    {
        public List<ConcertDTO> Concert { get; set; }

        public ConcertStub()
        {
            Concert = new List<ConcertDTO>()
            {
                new ConcertDTO() { ID = 1, BandID = 1, BandDTO = new BandDTO{ BandNaam = "Rammstein", BandInfo = "Test info"}, ConcertzaalID = 1, ConcertzaalDTO = new ConcertzaalDTO { ConcertzaalNaam = "013 Poppodium", Locatie = "Tilburg", Land = "Nederland" },Datum = new DateTime(2022-05-20), BeginTijd = "18:00", EindTijd = "20:00" },
                new ConcertDTO() { ID = 5, BandID = 6, ConcertzaalID = 3, BandDTO = new BandDTO{ BandNaam = "Slipknot", BandInfo = "Test info"}, ConcertzaalDTO = new ConcertzaalDTO { ConcertzaalNaam = "Ziggo Dome", Locatie = "Amsterdam", Land = "Nederland" }, Datum = new DateTime(2022-08-26), BeginTijd = "20:00", EindTijd = "22:00" },
                new ConcertDTO() { ID = 9, BandID = 4, ConcertzaalID = 8, BandDTO  = new BandDTO{ BandNaam = "Electric Callboy", BandInfo = "Test info"},ConcertzaalDTO = new ConcertzaalDTO { ConcertzaalNaam = "Trix", Locatie = "Antwerpen", Land = "Belgie" }, Datum = new DateTime(2023-02-11), BeginTijd = "19:30", EindTijd = "21:30" },
                new ConcertDTO() { ID = 9, BandID = 1, ConcertzaalID = 8, BandDTO  = new BandDTO{ BandNaam = "Rammstein", BandInfo = "Test info"},ConcertzaalDTO = new ConcertzaalDTO { ConcertzaalNaam = "Trix", Locatie = "Antwerpen", Land = "Belgie" }, Datum = new DateTime(2023-02-11), BeginTijd = "19:30", EindTijd = "21:30" }
            };
        }
        
        public void AddConcert (ConcertDTO concert)
        {
            Concert.Add(concert);
        }
        public void DeleteConcert (ConcertDTO concertdto)
        {
            ConcertDTO dto = new ConcertDTO();
            foreach(ConcertDTO concert in Concert) 
            {
                if(concertdto.ID == concert.ID) 
                {
                    dto = concert;
                }
            }

            Concert.Remove(dto);
        }
        public void UpdateConcert (ConcertDTO concert)
        {
            foreach (ConcertDTO concerts in Concert)
            {
                if (concerts.ID == concert.ID)
                {
                    concerts.BandID = concert.BandID;
                    concerts.ConcertzaalID = concert.ConcertzaalID;
                    concerts.Datum = concert.Datum;
                    concerts.BeginTijd = concert.BeginTijd;
                    concerts.EindTijd = concert.EindTijd;
                }
            }
        }
        public ConcertDTO GetByID(int ID)
        {
            foreach (ConcertDTO concerts in Concert)
            {
                if (concerts.ID == ID)
                {
                    return concerts;
                }
            }
            return new ConcertDTO();
        }
        public List<ConcertDTO> GetAll()
        {
            List<ConcertDTO> concerten = new List<ConcertDTO>();

            foreach (ConcertDTO concert in Concert)
            {
                concerten.Add(concert);
            }
            return Concert;
        }
        public List<ConcertDTO> GetConcertsByBand(int id)
        {
            List<ConcertDTO> concerten = new List<ConcertDTO>();
            foreach (ConcertDTO item in Concert)
            {
                if (item.BandID == id)
                {
                    concerten.Add(item);
                }
            }
            return concerten;
        }
        public List<ConcertDTO> GetConcertsByConcertzaal(int id)
        {
            List<ConcertDTO> concerten = new List<ConcertDTO>();
            foreach (ConcertDTO item in Concert)
            {
                if (item.ConcertzaalID == id)
                {
                    concerten.Add(item);
                }  
            }
            return concerten;
        }

    }
}
