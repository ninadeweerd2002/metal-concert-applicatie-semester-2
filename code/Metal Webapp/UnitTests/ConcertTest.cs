using Microsoft.VisualStudio.TestTools.UnitTesting;
using Metal_Webapp.Controllers;
using DAL;
using BLL;
using System;
using System.Collections.Generic;

namespace UnitTests
{

    [TestClass]
    public class ConcertTest
    {      
        private ConcertStub stub = new ConcertStub();
        private List<ConcertModel> concerts = new List<ConcertModel>();


        [TestMethod]
        public void ConcertGetAllTest()
        {
            ConcertContainer concertCon = new ConcertContainer(stub);
            concerts = concertCon.GetAll();

            Assert.IsNotNull(concerts);
            Assert.AreEqual(concerts.Count, 4);
        }

        [TestMethod]
        public void AddConcertenTest()
        {
            ConcertContainer concertCon = new ConcertContainer(stub);
            ConcertModel concert = new ConcertModel()
            {
                ID = 4,
                BandID = 15,
                ConcertzaalID = 1,
                BandNaam = "Test band",
                BandInfo = "Test info",
                Datum = new DateTime(2022-07-13),
                Locatie = "Tilburg",
                Land = "Nederland",
                BeginTijd = "19:45",
                EindTijd = "22:00"
                
            };
            concertCon.AddConcert(concert);

            Assert.AreEqual(stub.Concert[stub.Concert.Count - 1].ID, concert.ID);
            Assert.AreEqual(stub.Concert[stub.Concert.Count - 1].BandID, concert.BandID);           
            Assert.AreEqual(stub.Concert[stub.Concert.Count - 1].ConcertzaalID, concert.ConcertzaalID);
            Assert.AreEqual(stub.Concert[stub.Concert.Count - 1].Datum, concert.Datum);
            Assert.AreEqual(stub.Concert[stub.Concert.Count - 1].EindTijd, concert.EindTijd);
            Assert.AreEqual(stub.Concert[stub.Concert.Count - 1].BeginTijd, concert.BeginTijd);
        }
        [TestMethod]
        public void GetConcertByIDTest()
        {
            ConcertContainer concertCon = new ConcertContainer(stub);
            ConcertModel concert = concertCon.GetByID(9);

            Assert.IsNotNull(concert);
            Assert.AreEqual("Electric Callboy", stub.Concert[2].BandDTO.BandNaam);
            Assert.AreEqual("Electric Callboy", concert.BandNaam);
            Assert.AreEqual(stub.Concert[2].ID, concert.ID);
            
        }
        [TestMethod]
        public void DeleteConcertTest()
        {
            ConcertContainer concertCon = new ConcertContainer(stub);
            
            ConcertModel concert = new ConcertModel() { ID = 5, BandID = 6, ConcertzaalID = 3, Datum = new DateTime(2022 - 08 - 26), BeginTijd = "20:00", EindTijd = "22:00" };
            Assert.AreEqual(concert.ID, stub.Concert[1].ID);
            concertCon.DeleteConcert(concert);
            Assert.AreNotEqual(concert.ID, stub.Concert[1].ID);
        }

        [TestMethod]
        public void UpdateConcertTest()
        {
            ConcertContainer concertCon = new ConcertContainer(stub);

            ConcertModel concert = new ConcertModel()
            {
                ID = 5,
                BandID = 15,
                ConcertzaalID = 1,               
                Datum = new DateTime(2022 - 07 - 13),
                BeginTijd = "19:45",
                EindTijd = "22:00"
            };
            concertCon.UpdateConcert(concert);

            Assert.IsNotNull(concert);
            Assert.AreEqual(stub.Concert[1].ID, concert.ID);
            Assert.AreEqual(stub.Concert[1].BandID, concert.BandID);
            Assert.AreEqual(stub.Concert[1].ConcertzaalID, concert.ConcertzaalID);
            Assert.AreEqual(stub.Concert[1].Datum, concert.Datum);
            Assert.AreEqual(stub.Concert[1].BeginTijd, concert.BeginTijd);
            Assert.AreEqual(stub.Concert[1].EindTijd, concert.EindTijd);

        }

        [TestMethod]
        public void GetConcertsByBandTest()
        {
            ConcertContainer concertCon = new ConcertContainer(stub);

            List<ConcertModel> concert = concertCon.GetConcertsByBand(1);

            Assert.IsNotNull(concert);
            Assert.AreEqual(concert.Count, 2);
        }

        [TestMethod]
        public void GetConcertsByConcertzaalTest()
        {
            ConcertContainer concertCon = new ConcertContainer(stub);

            List<ConcertModel> concert = concertCon.GetConcertsByConcertzaal(3);

            Assert.IsNotNull(concert);
            Assert.AreEqual(concert.Count, 1);
        }
    }
}