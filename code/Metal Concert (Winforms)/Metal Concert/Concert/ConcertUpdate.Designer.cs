﻿namespace Metal_Concert
{
    partial class ConcertUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.concertzaalNaamDropdown = new System.Windows.Forms.ComboBox();
            this.bandNaamDropdown = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.altTekstInput = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tijdEindInput = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tijdBeginInput = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.datumInput = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listBand = new System.Windows.Forms.ListBox();
            this.concertUpdateButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // concertzaalNaamDropdown
            // 
            this.concertzaalNaamDropdown.FormattingEnabled = true;
            this.concertzaalNaamDropdown.Location = new System.Drawing.Point(668, 223);
            this.concertzaalNaamDropdown.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.concertzaalNaamDropdown.Name = "concertzaalNaamDropdown";
            this.concertzaalNaamDropdown.Size = new System.Drawing.Size(318, 28);
            this.concertzaalNaamDropdown.TabIndex = 29;
            // 
            // bandNaamDropdown
            // 
            this.bandNaamDropdown.FormattingEnabled = true;
            this.bandNaamDropdown.Location = new System.Drawing.Point(668, 93);
            this.bandNaamDropdown.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bandNaamDropdown.Name = "bandNaamDropdown";
            this.bandNaamDropdown.Size = new System.Drawing.Size(318, 28);
            this.bandNaamDropdown.TabIndex = 28;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(114, 412);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 20);
            this.label6.TabIndex = 27;
            this.label6.Text = "Alt-tekst:";
            // 
            // altTekstInput
            // 
            this.altTekstInput.Location = new System.Drawing.Point(118, 458);
            this.altTekstInput.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.altTekstInput.Name = "altTekstInput";
            this.altTekstInput.Size = new System.Drawing.Size(318, 27);
            this.altTekstInput.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(787, 458);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 20);
            this.label5.TabIndex = 25;
            this.label5.Text = "tot";
            // 
            // tijdEindInput
            // 
            this.tijdEindInput.Location = new System.Drawing.Point(832, 458);
            this.tijdEindInput.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tijdEindInput.Name = "tijdEindInput";
            this.tijdEindInput.Size = new System.Drawing.Size(100, 27);
            this.tijdEindInput.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(664, 412);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 20);
            this.label4.TabIndex = 23;
            this.label4.Text = "Tijd:";
            // 
            // tijdBeginInput
            // 
            this.tijdBeginInput.Location = new System.Drawing.Point(668, 458);
            this.tijdBeginInput.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tijdBeginInput.Name = "tijdBeginInput";
            this.tijdBeginInput.Size = new System.Drawing.Size(100, 27);
            this.tijdBeginInput.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(664, 296);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(183, 20);
            this.label3.TabIndex = 21;
            this.label3.Text = "Datum (DD-MM-JJJJ):";
            // 
            // datumInput
            // 
            this.datumInput.Location = new System.Drawing.Point(668, 341);
            this.datumInput.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.datumInput.Name = "datumInput";
            this.datumInput.Size = new System.Drawing.Size(179, 27);
            this.datumInput.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(664, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 20);
            this.label2.TabIndex = 19;
            this.label2.Text = "Naam band/artiest:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(664, 173);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 20);
            this.label1.TabIndex = 18;
            this.label1.Text = "Concertzaal:";
            // 
            // listBand
            // 
            this.listBand.FormattingEnabled = true;
            this.listBand.ItemHeight = 20;
            this.listBand.Location = new System.Drawing.Point(118, 105);
            this.listBand.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listBand.Name = "listBand";
            this.listBand.Size = new System.Drawing.Size(318, 224);
            this.listBand.TabIndex = 30;
            this.listBand.SelectedIndexChanged += new System.EventHandler(this.listBand_SelectedIndexChanged);
            // 
            // concertUpdateButton
            // 
            this.concertUpdateButton.Location = new System.Drawing.Point(664, 565);
            this.concertUpdateButton.Name = "concertUpdateButton";
            this.concertUpdateButton.Size = new System.Drawing.Size(196, 62);
            this.concertUpdateButton.TabIndex = 31;
            this.concertUpdateButton.Text = "Concert updaten";
            this.concertUpdateButton.UseVisualStyleBackColor = true;
            this.concertUpdateButton.Click += new System.EventHandler(this.concertUpdateButton_Click);
            // 
            // ConcertUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1101, 687);
            this.Controls.Add(this.concertUpdateButton);
            this.Controls.Add(this.listBand);
            this.Controls.Add(this.concertzaalNaamDropdown);
            this.Controls.Add(this.bandNaamDropdown);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.altTekstInput);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tijdEindInput);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tijdBeginInput);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.datumInput);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ConcertUpdate";
            this.Text = "ConcertUpdate";
            this.Load += new System.EventHandler(this.ConcertUpdate_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComboBox concertzaalNaamDropdown;
        private ComboBox bandNaamDropdown;
        private Label label6;
        private TextBox altTekstInput;
        private Label label5;
        private TextBox tijdEindInput;
        private Label label4;
        private TextBox tijdBeginInput;
        private Label label3;
        private TextBox datumInput;
        private Label label2;
        private Label label1;
        private ListBox listBand;
        private Button concertUpdateButton;
    }
}