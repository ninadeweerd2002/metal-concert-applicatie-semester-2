﻿namespace Metal_Concert
{
    partial class ConcertAanmaken
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.concertAanmakenButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.datumInput = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tijdBeginInput = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tijdEindInput = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.altTekstInput = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.bandNaamDropdown = new System.Windows.Forms.ComboBox();
            this.concertzaalNaamDropdown = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // concertAanmakenButton
            // 
            this.concertAanmakenButton.Location = new System.Drawing.Point(642, 492);
            this.concertAanmakenButton.Name = "concertAanmakenButton";
            this.concertAanmakenButton.Size = new System.Drawing.Size(171, 62);
            this.concertAanmakenButton.TabIndex = 0;
            this.concertAanmakenButton.Text = "Concert aanmaken";
            this.concertAanmakenButton.UseVisualStyleBackColor = true;
            this.concertAanmakenButton.Click += new System.EventHandler(this.concertAanmakenButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F);
            this.label1.Location = new System.Drawing.Point(638, 177);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Concertzaal:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F);
            this.label2.Location = new System.Drawing.Point(638, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Naam band/artiest:";
            // 
            // datumInput
            // 
            this.datumInput.Location = new System.Drawing.Point(642, 311);
            this.datumInput.Name = "datumInput";
            this.datumInput.Size = new System.Drawing.Size(179, 22);
            this.datumInput.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F);
            this.label3.Location = new System.Drawing.Point(638, 275);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(183, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Datum (DD-MM-JJJJ):";
            // 
            // tijdBeginInput
            // 
            this.tijdBeginInput.Location = new System.Drawing.Point(642, 405);
            this.tijdBeginInput.Name = "tijdBeginInput";
            this.tijdBeginInput.Size = new System.Drawing.Size(100, 22);
            this.tijdBeginInput.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F);
            this.label4.Location = new System.Drawing.Point(638, 368);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "Tijd:";
            // 
            // tijdEindInput
            // 
            this.tijdEindInput.Location = new System.Drawing.Point(806, 405);
            this.tijdEindInput.Name = "tijdEindInput";
            this.tijdEindInput.Size = new System.Drawing.Size(100, 22);
            this.tijdEindInput.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F);
            this.label5.Location = new System.Drawing.Point(761, 405);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "tot";
            // 
            // altTekstInput
            // 
            this.altTekstInput.Location = new System.Drawing.Point(92, 405);
            this.altTekstInput.Name = "altTekstInput";
            this.altTekstInput.Size = new System.Drawing.Size(318, 22);
            this.altTekstInput.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F);
            this.label6.Location = new System.Drawing.Point(88, 368);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 20);
            this.label6.TabIndex = 14;
            this.label6.Text = "Alt-tekst:";
            // 
            // bandNaamDropdown
            // 
            this.bandNaamDropdown.FormattingEnabled = true;
            this.bandNaamDropdown.Location = new System.Drawing.Point(642, 113);
            this.bandNaamDropdown.Name = "bandNaamDropdown";
            this.bandNaamDropdown.Size = new System.Drawing.Size(318, 24);
            this.bandNaamDropdown.TabIndex = 15;
            this.bandNaamDropdown.SelectedIndexChanged += new System.EventHandler(this.TestDropdown_SelectedIndexChanged);
            // 
            // concertzaalNaamDropdown
            // 
            this.concertzaalNaamDropdown.FormattingEnabled = true;
            this.concertzaalNaamDropdown.Location = new System.Drawing.Point(642, 217);
            this.concertzaalNaamDropdown.Name = "concertzaalNaamDropdown";
            this.concertzaalNaamDropdown.Size = new System.Drawing.Size(318, 24);
            this.concertzaalNaamDropdown.TabIndex = 16;
            // 
            // ConcertAanmaken
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 610);
            this.Controls.Add(this.concertzaalNaamDropdown);
            this.Controls.Add(this.bandNaamDropdown);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.altTekstInput);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tijdEindInput);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tijdBeginInput);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.datumInput);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.concertAanmakenButton);
            this.Name = "ConcertAanmaken";
            this.Text = "ConcertAanmaken";
            this.Load += new System.EventHandler(this.ConcertAanmaken_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button concertAanmakenButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox datumInput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tijdBeginInput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tijdEindInput;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox altTekstInput;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox bandNaamDropdown;
        private System.Windows.Forms.ComboBox concertzaalNaamDropdown;
    }
}