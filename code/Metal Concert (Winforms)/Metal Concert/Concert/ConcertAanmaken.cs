﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Metal_Concert
{
    public partial class ConcertAanmaken : Form
    {
        public ConcertAanmaken()
        {
            InitializeComponent();

        }

        private void concertAanmakenButton_Click(object sender, EventArgs e)
        {
            
            Concert c = new Concert((int)bandNaamDropdown.SelectedValue, (int)concertzaalNaamDropdown.SelectedValue, Convert.ToDateTime(datumInput.Text), tijdBeginInput.Text, tijdEindInput.Text);

            if (String.IsNullOrEmpty(bandNaamDropdown.Text) || String.IsNullOrEmpty(concertzaalNaamDropdown.Text) || String.IsNullOrEmpty(datumInput.Text) || String.IsNullOrEmpty(tijdBeginInput.Text) || String.IsNullOrEmpty(tijdEindInput.Text))
            {
                MessageBox.Show("Vul alle velden in!");
            }
            else {
                ConcertContainer container = new ConcertContainer();
                container.AddConcert(c);
                MessageBox.Show("Concert is aangemaakt!");
            }
        }


        private void ConcertAanmaken_Load(object sender, EventArgs e)
        {
           
            SqlConnection connection = new SqlConnection("Data Source=LAPTOP-VK0TD50R;Initial Catalog=Fontys metal;Integrated Security=True");
            connection.Open();

            SqlDataAdapter query = new SqlDataAdapter("SELECT * FROM bands", connection);
            SqlDataAdapter query2 = new SqlDataAdapter("SELECT * FROM concertzaal", connection);

            DataTable dt = new DataTable();
            query.Fill(dt);

            DataTable dt2 = new DataTable();
            query2.Fill(dt2);

            bandNaamDropdown.DisplayMember = "bandnaam";
            bandNaamDropdown.ValueMember = "idband";

            concertzaalNaamDropdown.DisplayMember = "concertzaalnaam";
            concertzaalNaamDropdown.ValueMember = "idconcertzaal";

            foreach (DataRow q in dt.Rows)
            {
                bandNaamDropdown.Items.Add(q[bandNaamDropdown.DisplayMember].ToString());
            }
            foreach (DataRow q in dt2.Rows)
            {
                concertzaalNaamDropdown.Items.Add(q[concertzaalNaamDropdown.DisplayMember].ToString());
            }

            bandNaamDropdown.DataSource = dt;
            concertzaalNaamDropdown.DataSource = dt2;
        }

        private void TestDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }

}
