﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DAL;

namespace Metal_Concert
{
    public class Concert
    {
        public int ID { get; set; }
        public int BandID { get; set; }
        public Band band { get; set; }
        public int ConcertzaalID { get; set; }
        public DateTime Datum { get; set; }
        public string BeginTijd { get; set; }
        public string EindTijd { get; set; }
        

        public Concert(int bandid, int concertzaalid, DateTime datum, string begintijd, string eindtijd)
        {
            BandID = bandid;
            ConcertzaalID = concertzaalid;
            Datum = datum;
            BeginTijd = begintijd;
            EindTijd = eindtijd;
        }
        public Concert(int id)
        {
            ID = id;
        }
        public Concert(ConcertDTO c)
        {
            ID = c.ID;
            BandID = c.BandID;
            band = new Band(c.BandDTO);
            ConcertzaalID = c.ConcertzaalID;
            Datum = c.Datum;
            BeginTijd = c.BeginTijd;
            EindTijd = c.EindTijd;
        }

    }
}
