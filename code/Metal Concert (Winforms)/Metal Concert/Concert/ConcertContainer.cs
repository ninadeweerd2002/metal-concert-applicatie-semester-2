﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metal_Concert
{
    public class ConcertContainer
    {

        public void AddConcert(Concert concert)
        {
            ConcertContext dal = new ConcertContext();
            ConcertDTO dto = new ConcertDTO(concert.ID, concert.BandID, concert.ConcertzaalID, concert.Datum, concert.BeginTijd, concert.EindTijd);
            dal.AddConcert(dto);
        }

        public void DeleteConcert(Concert concert)
        {
            ConcertContext dal = new ConcertContext();
            ConcertDTO dto = new ConcertDTO(concert.ID, concert.BandID, concert.ConcertzaalID, concert.Datum, concert.BeginTijd, concert.EindTijd);
            dal.DeleteConcert(dto);

        }
        public List<Concert> GetAll()
        {
            ConcertContext dals = new ConcertContext();

            List<ConcertDTO> dtoList = dals.GetAll();

            List<Concert> concerten = new List<Concert>();
            foreach (var item in dtoList)
            {
                concerten.Add(new Concert(item));
            }

            return concerten;
        }
    }
}
