﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Metal_Concert
{
    public partial class ConcertUpdate : Form
    {
        public ConcertUpdate()
        {
            InitializeComponent();
            
        }

        private void ConcertUpdate_Load(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection("Data Source=LAPTOP-VK0TD50R;Initial Catalog=Fontys metal;Integrated Security=True");
            connection.Open();

            SqlDataAdapter query = new SqlDataAdapter("SELECT concerten.id, bands.bandnaam, concertzaal.concertzaalnaam, concerten.datum, concerten.tijdbegin, concerten.tijdeinde FROM concerten JOIN bands ON bands.idband=concerten.idband JOIN concertzaal ON concertzaal.idconcertzaal=concerten.idconcertzaal", connection);

            DataTable dt = new DataTable();
            query.Fill(dt);

            listBand.Items.Clear();
            foreach (DataRow q in dt.Rows)
            {
                listBand.Items.Add(q["bandnaam"].ToString());
            }
            listBand.ValueMember = "id";
            bandNaamDropdown.ValueMember = "id";

            SqlDataAdapter query2 = new SqlDataAdapter("SELECT * FROM concertzaal", connection);

            DataTable dt2 = new DataTable();
            query2.Fill(dt2);

            concertzaalNaamDropdown.DisplayMember = "concertzaalnaam";
            concertzaalNaamDropdown.ValueMember = "idconcertzaal";

            foreach (DataRow q in dt2.Rows)
            {
                concertzaalNaamDropdown.Items.Add(q[concertzaalNaamDropdown.DisplayMember].ToString());
            }

        }

        private void concertUpdateButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(bandNaamDropdown.Text) || String.IsNullOrEmpty(concertzaalNaamDropdown.Text) || String.IsNullOrEmpty(datumInput.Text) || String.IsNullOrEmpty(tijdBeginInput.Text) || String.IsNullOrEmpty(tijdEindInput.Text))
            {
                MessageBox.Show("Vul alle velden in!");
            } else
            {
                SqlConnection connection = new SqlConnection("Data Source=LAPTOP-VK0TD50R;Initial Catalog=Fontys metal;Integrated Security=True");
                connection.Open();

                SqlCommand query = new SqlCommand("UPDATE concerten SET idconcertzaal=concertzaalNaamDropdown.ValueMember, datum=datumInput.Text, tijdbegin=tijdBeginInput.Text, tijdeinde=tijdEindInput.Text WHERE id=listBand.SelectedIndex", connection);
                
                query.ExecuteNonQuery();

                MessageBox.Show("Test");
            }
        }

        private void listBand_SelectedIndexChanged(object sender, EventArgs e)
        {
            bandNaamDropdown.Text = listBand.Text;
        }
    }
}
