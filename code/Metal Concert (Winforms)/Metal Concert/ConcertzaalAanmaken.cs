﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Metal_Concert
{
    public partial class ConcertzaalAanmaken : Form
    {
        public ConcertzaalAanmaken()
        {
            InitializeComponent();
        }

        private void ConcertzaalAanmakenButton_Click(object sender, EventArgs e)
        {
            Concertzalen c = new Concertzalen(concertzalenInputBox.Text, locatieInputBox.Text, landDropDown.Text);

            if (String.IsNullOrEmpty(concertzalenInputBox.Text) || String.IsNullOrEmpty(locatieInputBox.Text) || String.IsNullOrEmpty(landDropDown.Text))
            {
                MessageBox.Show("Vul alle velden in!");
            }
            else
            {
                ConcertzaalContainer container = new ConcertzaalContainer();
                container.AddConcertzaal(c);
                MessageBox.Show("Concertzaal is aangemaakt!");
            }
        }

        private void ConcertzaalAanmaken_Load_1(object sender, EventArgs e)
        {
            landDropDown.Items.Add("Nederland");
            landDropDown.Items.Add("België");
        }
    }
}
