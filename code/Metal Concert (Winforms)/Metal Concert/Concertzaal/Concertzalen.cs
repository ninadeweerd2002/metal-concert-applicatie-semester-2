﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace Metal_Concert
{
    public class Concertzalen
    {
        public int ConcertzaalID { get; set; }
        public string ConcertzaalNaam { get; set; }
        public string Locatie { get; set; }
        public string Land { get; set; }

        public Concertzalen(int concertzaalid, string concertzaalnaam, string locatie, string land)
        {
            ConcertzaalID = concertzaalid;
            ConcertzaalNaam = concertzaalnaam;
            Locatie = locatie;
            Land = land;
        }
        public Concertzalen(string concertzaalnaam, string locatie, string land)
        {
            ConcertzaalNaam = concertzaalnaam;
            Locatie = locatie;
            Land = land;
        }
        public Concertzalen(ConcertzaalDTO c)
        {
            ConcertzaalID = c.ConcertzaalID;
            ConcertzaalNaam = c.ConcertzaalNaam;
            Locatie = c.Locatie;
            Land = c.Land;
        }
        public Concertzalen(string concertzaalnaam)
        {
            ConcertzaalNaam = concertzaalnaam;
        }
        public Concertzalen(int concertzaalid)
        {
            ConcertzaalID = concertzaalid;
        }
    }
}
