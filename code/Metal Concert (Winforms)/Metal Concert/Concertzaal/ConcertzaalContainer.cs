﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace Metal_Concert
{
    public class ConcertzaalContainer
    {
        public void AddConcertzaal(Concertzalen concertzaal)
        {
            ConcertzaalContext dal = new ConcertzaalContext();
            ConcertzaalDTO dto = new ConcertzaalDTO(concertzaal.ConcertzaalID, concertzaal.ConcertzaalNaam, concertzaal.Locatie, concertzaal.Land);
            dal.AddConcertzaal(dto);
        }

        public void DeleteConcertzaal(Concertzalen concertzaal)
        {
            ConcertzaalContext dal = new ConcertzaalContext();
            ConcertzaalDTO dto = new ConcertzaalDTO(concertzaal.ConcertzaalID, concertzaal.ConcertzaalNaam, concertzaal.Locatie, concertzaal.Land);
            dal.DeleteConcertzaal(dto);

        }
        public List<Concertzalen> GetAll()
        {
            ConcertzaalContext dals = new ConcertzaalContext();

            List<ConcertzaalDTO> dtoList = dals.GetAll();

            List<Concertzalen> concertzaal = new List<Concertzalen>();
            foreach (var item in dtoList)
            {
                concertzaal.Add(new Concertzalen(item));
            }

            return concertzaal;
        }
    }
}
