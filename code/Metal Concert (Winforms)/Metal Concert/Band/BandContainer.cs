﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace Metal_Concert
{
    public class BandContainer
    {
        public void AddBand(Band band)
        {
            BandContext dal = new BandContext();
            BandDTO dto = new BandDTO(band.BandID, band.BandNaam);
            dal.AddBand(dto);
        }

        public void DeleteBand(Band band)
        {
            BandContext dal = new BandContext();
            BandDTO dto = new BandDTO(band.BandID, band.BandNaam);
            dal.DeleteBand(dto);

        }
        public List<Band> GetAll()
        {
            BandContext dals = new BandContext();

            List<BandDTO> dtoList = dals.GetAll();

            List<Band> bands = new List<Band>();
            foreach (var item in dtoList)
            {
                bands.Add(new Band(item));
            }

            return bands;
        }
    }
}
