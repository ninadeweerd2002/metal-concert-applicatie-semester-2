﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace Metal_Concert
{
    public class Band
    {

        public int BandID { get; set; }
        public string BandNaam { get; set; }

        public Band(string bandnaam)
        { 
            BandNaam = bandnaam;
        }

        public Band(BandDTO b)
        {
            BandID = b.BandID;
            BandNaam = b.BandNaam;
        }

        public Band(int bandid)
        {
            BandID = bandid;
        }
    }
}
