﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Metal_Concert
{
    public partial class Inlogpagina : Form
    {
        public Inlogpagina()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void inlogButton_Click(object sender, EventArgs e)
        {
            string conn = "Data Source=LAPTOP-VK0TD50R;Initial Catalog=Fontys metal;Integrated Security=True";
            string query = "SELECT * FROM gebruikers WHERE username=@username AND password=@password";

            if (String.IsNullOrEmpty(usernameInputBox.Text) || String.IsNullOrEmpty(passwordInputBox.Text))
            {
                MessageBox.Show("Vul alle velden in!");
            }else
            {
                SqlConnection connection = new SqlConnection(conn);
                connection.Open();

                SqlCommand q = new SqlCommand(query, connection);
                q.Parameters.AddWithValue("@username", usernameInputBox.Text);
                q.Parameters.AddWithValue("@password", passwordInputBox.Text);

                var dataRow = q.ExecuteReader();

                if (dataRow.Read())
                {
                    MessageBox.Show("Inloggen gelukt!");
                    ConcertAanmaken a = new ConcertAanmaken();
                    this.Close();
                    a.Show();
                    
                }
                else
                {
                    MessageBox.Show("Inloggen mislukt!");
                }
            }
        }

        private void registratieLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Registratie regLink = new Registratie();
            regLink.Show();
        }
    }
}
