﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace Metal_Concert
{
    public class GebruikerContainer
    {
        public void AddGebruiker(Gebruiker gebruiker)
        {
            GebruikerContext dal = new GebruikerContext();
            GebruikerDTO dto = new GebruikerDTO(gebruiker.Gebruikersnaam, gebruiker.Email, gebruiker.Password);
            dal.AddGebruiker(dto);
        }

    }
}
