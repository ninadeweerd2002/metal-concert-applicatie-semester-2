﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Metal_Concert
{
    public partial class AdminOverzicht : Form
    {
        public AdminOverzicht()
        {
            InitializeComponent();
        }

        private void updateConcertsList_Click(object sender, EventArgs e)
        {

            ConcertContainer container = new ConcertContainer();
            List<Concert> concerten = container.GetAll();
       

            showConcerten.DataSource = concerten;  

        }

        private void addConcert_Click(object sender, EventArgs e)
        {
            ConcertAanmaken aanmaken = new ConcertAanmaken();
            aanmaken.Show();
        }


        private void showConcerten_CellContentClick(object sender, DataGridViewCellEventArgs e)
        { 
            var id = Convert.ToInt32(this.showConcerten.Rows[e.RowIndex].Cells[2].Value);
            Concert c = new Concert(id);

            if (showConcerten.Columns[e.ColumnIndex].Name == "DeleteButton")
            { 
                ConcertContainer container = new ConcertContainer();
                container.DeleteConcert(c);
                MessageBox.Show("Concert is verwijderd!");
            }
        }

        private void updateBandsList_Click(object sender, EventArgs e)
        {
            BandContainer container = new BandContainer();
            List<Band> bands = container.GetAll();

            showBands.DataSource = bands;
        }

        private void addBand_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(bandInput.Text))
            {
                Band b = new Band(bandInput.Text);
                BandContainer container = new BandContainer();
                container.AddBand(b);
                MessageBox.Show("Band is aangemaakt!");
            } else
            {
                MessageBox.Show("Vul een band in!");
            }
            
        }

        private void showBands_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var id = Convert.ToInt32(this.showBands.Rows[e.RowIndex].Cells[2].Value);
            Band b = new Band(id);

            if (showBands.Columns[e.ColumnIndex].Name == "DeleteButtonBand")
            {
                BandContainer container = new BandContainer();
                container.DeleteBand(b);
                MessageBox.Show("Band is verwijderd!");
            }
        }

        private void UpdateConcertzalen_Click(object sender, EventArgs e)
        {
            ConcertzaalContainer container = new ConcertzaalContainer();
            List<Concertzalen> concertzalen = container.GetAll();

            showConcertzalen.DataSource = concertzalen;
        }

        private void AddConcertzaal_Click(object sender, EventArgs e)
        {
            ConcertzaalAanmaken aanmaken = new ConcertzaalAanmaken();
            aanmaken.Show();
        }

        private void showConcertzalen_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var id = Convert.ToInt32(this.showConcertzalen.Rows[e.RowIndex].Cells[2].Value);
            Concertzalen b = new Concertzalen(id);

            if (showConcertzalen.Columns[e.ColumnIndex].Name == "DeleteButtonConcertzalen")
            {
                ConcertzaalContainer container = new ConcertzaalContainer();
                container.DeleteConcertzaal(b);
                MessageBox.Show("Concertzaal is verwijderd!");
            }
        }
    }
}
