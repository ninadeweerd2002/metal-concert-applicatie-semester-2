﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace Metal_Concert
{
    public class Gebruiker
    {
        public string Gebruikersnaam { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public enum Rol
        {
            Bezoeker,
            Administrator
        }

        public Gebruiker(string gb, string email, string pw)
        {
            Gebruikersnaam = gb;
            Email = email;
            Password = pw;
        }

        public Gebruiker(GebruikerDTO g)
        {
            Gebruikersnaam = g.Gebruikersnaam;
            Email = g.Email;
            Password = g.Password;
        }
    }
}
