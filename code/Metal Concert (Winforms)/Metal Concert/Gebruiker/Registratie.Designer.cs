﻿namespace Metal_Concert
{
    partial class Registratie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.regUsername = new System.Windows.Forms.TextBox();
            this.regEmail = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.regPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.regPasswordConfirm = new System.Windows.Forms.TextBox();
            this.registrerenButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label1.Location = new System.Drawing.Point(114, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Gebruikersnaam:";
            // 
            // regUsername
            // 
            this.regUsername.Location = new System.Drawing.Point(117, 98);
            this.regUsername.Name = "regUsername";
            this.regUsername.Size = new System.Drawing.Size(214, 22);
            this.regUsername.TabIndex = 1;
            // 
            // regEmail
            // 
            this.regEmail.Location = new System.Drawing.Point(117, 188);
            this.regEmail.Name = "regEmail";
            this.regEmail.Size = new System.Drawing.Size(214, 22);
            this.regEmail.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label2.Location = new System.Drawing.Point(114, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Email:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label3.Location = new System.Drawing.Point(114, 276);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Wachtwoord:";
            // 
            // regPassword
            // 
            this.regPassword.Location = new System.Drawing.Point(117, 317);
            this.regPassword.Name = "regPassword";
            this.regPassword.PasswordChar = '*';
            this.regPassword.Size = new System.Drawing.Size(214, 22);
            this.regPassword.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label4.Location = new System.Drawing.Point(114, 370);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Herhaal wachtwoord:";
            // 
            // regPasswordConfirm
            // 
            this.regPasswordConfirm.Location = new System.Drawing.Point(117, 413);
            this.regPasswordConfirm.Name = "regPasswordConfirm";
            this.regPasswordConfirm.PasswordChar = '*';
            this.regPasswordConfirm.Size = new System.Drawing.Size(214, 22);
            this.regPasswordConfirm.TabIndex = 7;
            // 
            // registrerenButton
            // 
            this.registrerenButton.Location = new System.Drawing.Point(122, 489);
            this.registrerenButton.Name = "registrerenButton";
            this.registrerenButton.Size = new System.Drawing.Size(145, 38);
            this.registrerenButton.TabIndex = 8;
            this.registrerenButton.Text = "Registreren";
            this.registrerenButton.UseVisualStyleBackColor = true;
            this.registrerenButton.Click += new System.EventHandler(this.registrerenButton_Click);
            // 
            // Registratie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 583);
            this.Controls.Add(this.registrerenButton);
            this.Controls.Add(this.regPasswordConfirm);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.regPassword);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.regEmail);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.regUsername);
            this.Controls.Add(this.label1);
            this.Name = "Registratie";
            this.Text = "Registratie";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox regUsername;
        private System.Windows.Forms.TextBox regEmail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox regPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox regPasswordConfirm;
        private System.Windows.Forms.Button registrerenButton;
    }
}