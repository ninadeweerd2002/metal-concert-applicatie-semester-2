﻿namespace Metal_Concert
{
    partial class AdminOverzicht
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bandInput = new System.Windows.Forms.TextBox();
            this.showBands = new System.Windows.Forms.DataGridView();
            this.EditButtonBand = new System.Windows.Forms.DataGridViewButtonColumn();
            this.DeleteButtonBand = new System.Windows.Forms.DataGridViewButtonColumn();
            this.addBand = new System.Windows.Forms.Button();
            this.updateBandsList = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.libraryGroup = new System.Windows.Forms.GroupBox();
            this.showConcerten = new System.Windows.Forms.DataGridView();
            this.EditButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.DeleteButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.addConcert = new System.Windows.Forms.Button();
            this.updateConcertsList = new System.Windows.Forms.Button();
            this.ConcertenTab = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.Concertzalen = new System.Windows.Forms.GroupBox();
            this.showConcertzalen = new System.Windows.Forms.DataGridView();
            this.EditButtonConcertzalen = new System.Windows.Forms.DataGridViewButtonColumn();
            this.DeleteButtonConcertzalen = new System.Windows.Forms.DataGridViewButtonColumn();
            this.AddConcertzaal = new System.Windows.Forms.Button();
            this.UpdateConcertzalen = new System.Windows.Forms.Button();
            this.concertBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.concertContainerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.showBands)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.libraryGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.showConcerten)).BeginInit();
            this.ConcertenTab.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.Concertzalen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.showConcertzalen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.concertBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.concertContainerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(908, 636);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Bands";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bandInput);
            this.groupBox1.Controls.Add(this.showBands);
            this.groupBox1.Controls.Add(this.addBand);
            this.groupBox1.Controls.Add(this.updateBandsList);
            this.groupBox1.Location = new System.Drawing.Point(51, 22);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(809, 473);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bands";
            // 
            // bandInput
            // 
            this.bandInput.Location = new System.Drawing.Point(573, 405);
            this.bandInput.Name = "bandInput";
            this.bandInput.Size = new System.Drawing.Size(215, 27);
            this.bandInput.TabIndex = 7;
            // 
            // showBands
            // 
            this.showBands.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.showBands.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EditButtonBand,
            this.DeleteButtonBand});
            this.showBands.Location = new System.Drawing.Point(19, 81);
            this.showBands.Name = "showBands";
            this.showBands.RowHeadersWidth = 51;
            this.showBands.RowTemplate.Height = 29;
            this.showBands.Size = new System.Drawing.Size(769, 280);
            this.showBands.TabIndex = 6;
            this.showBands.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.showBands_CellContentClick);
            // 
            // EditButtonBand
            // 
            this.EditButtonBand.HeaderText = "Edit";
            this.EditButtonBand.MinimumWidth = 6;
            this.EditButtonBand.Name = "EditButtonBand";
            this.EditButtonBand.Text = "Edit";
            this.EditButtonBand.Width = 50;
            // 
            // DeleteButtonBand
            // 
            this.DeleteButtonBand.HeaderText = "Del";
            this.DeleteButtonBand.MinimumWidth = 6;
            this.DeleteButtonBand.Name = "DeleteButtonBand";
            this.DeleteButtonBand.Text = "Delete";
            this.DeleteButtonBand.Width = 50;
            // 
            // addBand
            // 
            this.addBand.Location = new System.Drawing.Point(310, 391);
            this.addBand.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.addBand.Name = "addBand";
            this.addBand.Size = new System.Drawing.Size(228, 54);
            this.addBand.TabIndex = 3;
            this.addBand.Text = "Add Band";
            this.addBand.UseVisualStyleBackColor = true;
            this.addBand.Click += new System.EventHandler(this.addBand_Click);
            // 
            // updateBandsList
            // 
            this.updateBandsList.Location = new System.Drawing.Point(19, 391);
            this.updateBandsList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.updateBandsList.Name = "updateBandsList";
            this.updateBandsList.Size = new System.Drawing.Size(199, 55);
            this.updateBandsList.TabIndex = 1;
            this.updateBandsList.Text = "Update Bands lijst";
            this.updateBandsList.UseVisualStyleBackColor = true;
            this.updateBandsList.Click += new System.EventHandler(this.updateBandsList_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.libraryGroup);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(908, 636);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Concerten";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // libraryGroup
            // 
            this.libraryGroup.Controls.Add(this.showConcerten);
            this.libraryGroup.Controls.Add(this.addConcert);
            this.libraryGroup.Controls.Add(this.updateConcertsList);
            this.libraryGroup.Location = new System.Drawing.Point(49, 22);
            this.libraryGroup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.libraryGroup.Name = "libraryGroup";
            this.libraryGroup.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.libraryGroup.Size = new System.Drawing.Size(809, 473);
            this.libraryGroup.TabIndex = 3;
            this.libraryGroup.TabStop = false;
            this.libraryGroup.Text = "Concerten";
            // 
            // showConcerten
            // 
            this.showConcerten.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.showConcerten.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EditButton,
            this.DeleteButton});
            this.showConcerten.Location = new System.Drawing.Point(19, 81);
            this.showConcerten.Name = "showConcerten";
            this.showConcerten.RowHeadersWidth = 51;
            this.showConcerten.RowTemplate.Height = 29;
            this.showConcerten.Size = new System.Drawing.Size(769, 280);
            this.showConcerten.TabIndex = 6;
            this.showConcerten.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.showConcerten_CellContentClick);
            // 
            // EditButton
            // 
            this.EditButton.HeaderText = "Edit";
            this.EditButton.MinimumWidth = 6;
            this.EditButton.Name = "EditButton";
            this.EditButton.Text = "Edit";
            this.EditButton.Width = 50;
            // 
            // DeleteButton
            // 
            this.DeleteButton.HeaderText = "Del";
            this.DeleteButton.MinimumWidth = 6;
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Text = "Delete";
            this.DeleteButton.Width = 50;
            // 
            // addConcert
            // 
            this.addConcert.Location = new System.Drawing.Point(560, 392);
            this.addConcert.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.addConcert.Name = "addConcert";
            this.addConcert.Size = new System.Drawing.Size(228, 54);
            this.addConcert.TabIndex = 3;
            this.addConcert.Text = "Add Concert";
            this.addConcert.UseVisualStyleBackColor = true;
            this.addConcert.Click += new System.EventHandler(this.addConcert_Click);
            // 
            // updateConcertsList
            // 
            this.updateConcertsList.Location = new System.Drawing.Point(19, 391);
            this.updateConcertsList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.updateConcertsList.Name = "updateConcertsList";
            this.updateConcertsList.Size = new System.Drawing.Size(199, 55);
            this.updateConcertsList.TabIndex = 1;
            this.updateConcertsList.Text = "Update Concerten lijst";
            this.updateConcertsList.UseVisualStyleBackColor = true;
            this.updateConcertsList.Click += new System.EventHandler(this.updateConcertsList_Click);
            // 
            // ConcertenTab
            // 
            this.ConcertenTab.Controls.Add(this.tabPage1);
            this.ConcertenTab.Controls.Add(this.tabPage2);
            this.ConcertenTab.Controls.Add(this.tabPage3);
            this.ConcertenTab.Location = new System.Drawing.Point(1, 2);
            this.ConcertenTab.Name = "ConcertenTab";
            this.ConcertenTab.SelectedIndex = 0;
            this.ConcertenTab.Size = new System.Drawing.Size(916, 669);
            this.ConcertenTab.TabIndex = 4;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.Concertzalen);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(908, 636);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Concertzalen";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // Concertzalen
            // 
            this.Concertzalen.Controls.Add(this.showConcertzalen);
            this.Concertzalen.Controls.Add(this.AddConcertzaal);
            this.Concertzalen.Controls.Add(this.UpdateConcertzalen);
            this.Concertzalen.Location = new System.Drawing.Point(47, 22);
            this.Concertzalen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Concertzalen.Name = "Concertzalen";
            this.Concertzalen.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Concertzalen.Size = new System.Drawing.Size(809, 473);
            this.Concertzalen.TabIndex = 4;
            this.Concertzalen.TabStop = false;
            this.Concertzalen.Text = "Concertzalen";
            // 
            // showConcertzalen
            // 
            this.showConcertzalen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.showConcertzalen.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EditButtonConcertzalen,
            this.DeleteButtonConcertzalen});
            this.showConcertzalen.Location = new System.Drawing.Point(19, 81);
            this.showConcertzalen.Name = "showConcertzalen";
            this.showConcertzalen.RowHeadersWidth = 51;
            this.showConcertzalen.RowTemplate.Height = 29;
            this.showConcertzalen.Size = new System.Drawing.Size(769, 280);
            this.showConcertzalen.TabIndex = 6;
            this.showConcertzalen.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.showConcertzalen_CellContentClick);
            // 
            // EditButtonConcertzalen
            // 
            this.EditButtonConcertzalen.HeaderText = "Edit";
            this.EditButtonConcertzalen.MinimumWidth = 6;
            this.EditButtonConcertzalen.Name = "EditButtonConcertzalen";
            this.EditButtonConcertzalen.Text = "Edit";
            this.EditButtonConcertzalen.Width = 50;
            // 
            // DeleteButtonConcertzalen
            // 
            this.DeleteButtonConcertzalen.HeaderText = "Del";
            this.DeleteButtonConcertzalen.MinimumWidth = 6;
            this.DeleteButtonConcertzalen.Name = "DeleteButtonConcertzalen";
            this.DeleteButtonConcertzalen.Text = "Delete";
            this.DeleteButtonConcertzalen.Width = 50;
            // 
            // AddConcertzaal
            // 
            this.AddConcertzaal.Location = new System.Drawing.Point(560, 391);
            this.AddConcertzaal.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AddConcertzaal.Name = "AddConcertzaal";
            this.AddConcertzaal.Size = new System.Drawing.Size(228, 54);
            this.AddConcertzaal.TabIndex = 3;
            this.AddConcertzaal.Text = "Add Concertzaal";
            this.AddConcertzaal.UseVisualStyleBackColor = true;
            this.AddConcertzaal.Click += new System.EventHandler(this.AddConcertzaal_Click);
            // 
            // UpdateConcertzalen
            // 
            this.UpdateConcertzalen.Location = new System.Drawing.Point(19, 391);
            this.UpdateConcertzalen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UpdateConcertzalen.Name = "UpdateConcertzalen";
            this.UpdateConcertzalen.Size = new System.Drawing.Size(199, 55);
            this.UpdateConcertzalen.TabIndex = 1;
            this.UpdateConcertzalen.Text = "Update Concertzalen lijst";
            this.UpdateConcertzalen.UseVisualStyleBackColor = true;
            this.UpdateConcertzalen.Click += new System.EventHandler(this.UpdateConcertzalen_Click);
            // 
            // concertBindingSource
            // 
            this.concertBindingSource.DataSource = typeof(Metal_Concert.Concert);
            // 
            // concertContainerBindingSource
            // 
            this.concertContainerBindingSource.DataSource = typeof(Metal_Concert.ConcertContainer);
            // 
            // AdminOverzicht
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 539);
            this.Controls.Add(this.ConcertenTab);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AdminOverzicht";
            this.Text = "Admin Overzicht";
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.showBands)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.libraryGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.showConcerten)).EndInit();
            this.ConcertenTab.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.Concertzalen.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.showConcertzalen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.concertBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.concertContainerBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TabPage tabPage2;
        private TabPage tabPage1;
        private GroupBox libraryGroup;
        private DataGridView showConcerten;
        private DataGridViewButtonColumn EditButton;
        private DataGridViewButtonColumn DeleteButton;
        private Button addConcert;
        private Button updateConcertsList;
        private TabControl ConcertenTab;
        private TabPage tabPage3;
        private GroupBox groupBox1;
        private DataGridView showBands;
        private Button addBand;
        private Button updateBandsList;
        private TextBox bandInput;
        private DataGridViewButtonColumn EditButtonBand;
        private DataGridViewButtonColumn DeleteButtonBand;
        private BindingSource concertBindingSource;
        private BindingSource concertContainerBindingSource;
        private GroupBox Concertzalen;
        private DataGridView showConcertzalen;
        private Button AddConcertzaal;
        private Button UpdateConcertzalen;
        private DataGridViewButtonColumn EditButtonConcertzalen;
        private DataGridViewButtonColumn DeleteButtonConcertzalen;
    }
}