﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Metal_Concert
{
    public partial class Overzicht : Form
    {
        public Overzicht()
        {
            InitializeComponent();
        }

        private void inlogButton_Click(object sender, EventArgs e)
        {
            Inlogpagina loginPage = new Inlogpagina();
            loginPage.Show();
        }

        private void registrerenButton_Click(object sender, EventArgs e)
        {
            Registratie regPage = new Registratie();
            regPage.Show();
        }
    }
}
