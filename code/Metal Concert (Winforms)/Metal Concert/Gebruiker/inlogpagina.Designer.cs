﻿namespace Metal_Concert
{
    partial class Inlogpagina
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usernameInputBox = new System.Windows.Forms.TextBox();
            this.passwordInputBox = new System.Windows.Forms.TextBox();
            this.labelUsername = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.inlogButton = new System.Windows.Forms.Button();
            this.registratieLink = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.logoTemp = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // usernameInputBox
            // 
            this.usernameInputBox.Location = new System.Drawing.Point(116, 159);
            this.usernameInputBox.Name = "usernameInputBox";
            this.usernameInputBox.Size = new System.Drawing.Size(264, 22);
            this.usernameInputBox.TabIndex = 0;
            // 
            // passwordInputBox
            // 
            this.passwordInputBox.Location = new System.Drawing.Point(116, 262);
            this.passwordInputBox.Name = "passwordInputBox";
            this.passwordInputBox.PasswordChar = '*';
            this.passwordInputBox.Size = new System.Drawing.Size(264, 22);
            this.passwordInputBox.TabIndex = 1;
            // 
            // labelUsername
            // 
            this.labelUsername.AutoSize = true;
            this.labelUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F);
            this.labelUsername.ForeColor = System.Drawing.Color.White;
            this.labelUsername.Location = new System.Drawing.Point(112, 127);
            this.labelUsername.Name = "labelUsername";
            this.labelUsername.Size = new System.Drawing.Size(91, 20);
            this.labelUsername.TabIndex = 2;
            this.labelUsername.Text = "Username:";
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F);
            this.labelPassword.ForeColor = System.Drawing.Color.White;
            this.labelPassword.Location = new System.Drawing.Point(112, 230);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(88, 20);
            this.labelPassword.TabIndex = 3;
            this.labelPassword.Text = "Password:";
            // 
            // inlogButton
            // 
            this.inlogButton.Location = new System.Drawing.Point(116, 330);
            this.inlogButton.Name = "inlogButton";
            this.inlogButton.Size = new System.Drawing.Size(100, 39);
            this.inlogButton.TabIndex = 4;
            this.inlogButton.Text = "Inloggen";
            this.inlogButton.UseVisualStyleBackColor = true;
            this.inlogButton.Click += new System.EventHandler(this.inlogButton_Click);
            // 
            // registratieLink
            // 
            this.registratieLink.ActiveLinkColor = System.Drawing.Color.Gray;
            this.registratieLink.AutoSize = true;
            this.registratieLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.8F);
            this.registratieLink.ForeColor = System.Drawing.Color.OrangeRed;
            this.registratieLink.LinkColor = System.Drawing.Color.Black;
            this.registratieLink.Location = new System.Drawing.Point(266, 409);
            this.registratieLink.Name = "registratieLink";
            this.registratieLink.Size = new System.Drawing.Size(114, 18);
            this.registratieLink.TabIndex = 5;
            this.registratieLink.TabStop = true;
            this.registratieLink.Text = "Meld je dan aan!";
            this.registratieLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.registratieLink_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.8F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(113, 409);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "Nog geen account?";
            // 
            // logoTemp
            // 
            this.logoTemp.AutoSize = true;
            this.logoTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 30.8F);
            this.logoTemp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.logoTemp.Location = new System.Drawing.Point(61, 38);
            this.logoTemp.Name = "logoTemp";
            this.logoTemp.Size = new System.Drawing.Size(373, 59);
            this.logoTemp.TabIndex = 7;
            this.logoTemp.Text = "Metal Concerts";
            // 
            // Inlogpagina
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(501, 501);
            this.Controls.Add(this.logoTemp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.registratieLink);
            this.Controls.Add(this.inlogButton);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.labelUsername);
            this.Controls.Add(this.passwordInputBox);
            this.Controls.Add(this.usernameInputBox);
            this.Name = "Inlogpagina";
            this.Text = "Inlogpagina";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox usernameInputBox;
        private System.Windows.Forms.TextBox passwordInputBox;
        private System.Windows.Forms.Label labelUsername;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Button inlogButton;
        private System.Windows.Forms.LinkLabel registratieLink;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label logoTemp;
    }
}

