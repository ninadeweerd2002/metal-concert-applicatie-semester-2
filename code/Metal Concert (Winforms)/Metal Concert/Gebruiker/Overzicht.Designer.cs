﻿namespace Metal_Concert
{
    partial class Overzicht
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.overzichtTab = new System.Windows.Forms.TabControl();
            this.concertOverzicht = new System.Windows.Forms.TabPage();
            this.loginPage = new System.Windows.Forms.TabPage();
            this.inlogButton = new System.Windows.Forms.Button();
            this.registrerenButton = new System.Windows.Forms.Button();
            this.overzichtTab.SuspendLayout();
            this.loginPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // overzichtTab
            // 
            this.overzichtTab.Controls.Add(this.concertOverzicht);
            this.overzichtTab.Controls.Add(this.loginPage);
            this.overzichtTab.Location = new System.Drawing.Point(12, 12);
            this.overzichtTab.Name = "overzichtTab";
            this.overzichtTab.SelectedIndex = 0;
            this.overzichtTab.Size = new System.Drawing.Size(856, 495);
            this.overzichtTab.TabIndex = 1;
            // 
            // concertOverzicht
            // 
            this.concertOverzicht.Location = new System.Drawing.Point(4, 29);
            this.concertOverzicht.Name = "concertOverzicht";
            this.concertOverzicht.Padding = new System.Windows.Forms.Padding(3);
            this.concertOverzicht.Size = new System.Drawing.Size(848, 462);
            this.concertOverzicht.TabIndex = 0;
            this.concertOverzicht.Text = "Concerten";
            this.concertOverzicht.UseVisualStyleBackColor = true;
            // 
            // loginPage
            // 
            this.loginPage.Controls.Add(this.registrerenButton);
            this.loginPage.Controls.Add(this.inlogButton);
            this.loginPage.Location = new System.Drawing.Point(4, 29);
            this.loginPage.Name = "loginPage";
            this.loginPage.Padding = new System.Windows.Forms.Padding(3);
            this.loginPage.Size = new System.Drawing.Size(848, 462);
            this.loginPage.TabIndex = 1;
            this.loginPage.Text = "Login";
            this.loginPage.UseVisualStyleBackColor = true;
            // 
            // inlogButton
            // 
            this.inlogButton.Location = new System.Drawing.Point(332, 157);
            this.inlogButton.Name = "inlogButton";
            this.inlogButton.Size = new System.Drawing.Size(167, 62);
            this.inlogButton.TabIndex = 0;
            this.inlogButton.Text = "Inloggen";
            this.inlogButton.UseVisualStyleBackColor = true;
            this.inlogButton.Click += new System.EventHandler(this.inlogButton_Click);
            // 
            // registrerenButton
            // 
            this.registrerenButton.Location = new System.Drawing.Point(332, 250);
            this.registrerenButton.Name = "registrerenButton";
            this.registrerenButton.Size = new System.Drawing.Size(167, 62);
            this.registrerenButton.TabIndex = 1;
            this.registrerenButton.Text = "Registreren";
            this.registrerenButton.UseVisualStyleBackColor = true;
            this.registrerenButton.Click += new System.EventHandler(this.registrerenButton_Click);
            // 
            // Overzicht
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 519);
            this.Controls.Add(this.overzichtTab);
            this.Name = "Overzicht";
            this.Text = "Overzicht";
            this.overzichtTab.ResumeLayout(false);
            this.loginPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl overzichtTab;
        private TabPage concertOverzicht;
        private TabPage loginPage;
        private Button registrerenButton;
        private Button inlogButton;
    }
}