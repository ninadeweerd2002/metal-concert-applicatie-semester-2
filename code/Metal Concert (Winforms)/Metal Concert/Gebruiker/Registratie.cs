﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Metal_Concert
{
    public partial class Registratie : Form
    {
        public Registratie()
        {
            InitializeComponent();
        }

        private void registrerenButton_Click(object sender, EventArgs e)
        {
            Gebruiker g = new Gebruiker(regUsername.Text, regEmail.Text, regPassword.Text);

            if (String.IsNullOrEmpty(regUsername.Text) || String.IsNullOrEmpty(regEmail.Text) || String.IsNullOrEmpty(regPassword.Text) || String.IsNullOrEmpty(regPasswordConfirm.Text))
            {
                MessageBox.Show("Vul alle velden in!");
            }
            else
            {
                if (regPassword.Text != regPasswordConfirm.Text)
                {
                    MessageBox.Show("Wachtwoorden komen niet overeen");
                }
                else
                {
                    GebruikerContainer container = new GebruikerContainer();
                    container.AddGebruiker(g);
                    MessageBox.Show("Nieuwe gebruiker is aangemaakt!");
                    this.Close();
                }
            }

        }
    }
}
