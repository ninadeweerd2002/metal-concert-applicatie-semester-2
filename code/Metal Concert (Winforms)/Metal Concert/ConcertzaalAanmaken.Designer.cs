﻿namespace Metal_Concert
{
    partial class ConcertzaalAanmaken
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ConcertzaalAanmakenButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.landDropDown = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.locatieInputBox = new System.Windows.Forms.TextBox();
            this.concertzalenInputBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ConcertzaalAanmakenButton
            // 
            this.ConcertzaalAanmakenButton.Location = new System.Drawing.Point(143, 454);
            this.ConcertzaalAanmakenButton.Name = "ConcertzaalAanmakenButton";
            this.ConcertzaalAanmakenButton.Size = new System.Drawing.Size(196, 68);
            this.ConcertzaalAanmakenButton.TabIndex = 0;
            this.ConcertzaalAanmakenButton.Text = "Concertzaal Aanmaken";
            this.ConcertzaalAanmakenButton.UseVisualStyleBackColor = true;
            this.ConcertzaalAanmakenButton.Click += new System.EventHandler(this.ConcertzaalAanmakenButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(79, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 20);
            this.label2.TabIndex = 18;
            this.label2.Text = "Naam concertzaal:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(79, 189);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 17;
            this.label1.Text = "Locatie:";
            // 
            // landDropDown
            // 
            this.landDropDown.FormattingEnabled = true;
            this.landDropDown.Location = new System.Drawing.Point(87, 373);
            this.landDropDown.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.landDropDown.Name = "landDropDown";
            this.landDropDown.Size = new System.Drawing.Size(318, 28);
            this.landDropDown.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(83, 323);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 21;
            this.label3.Text = "Land:";
            // 
            // locatieInputBox
            // 
            this.locatieInputBox.Location = new System.Drawing.Point(83, 256);
            this.locatieInputBox.Name = "locatieInputBox";
            this.locatieInputBox.Size = new System.Drawing.Size(318, 27);
            this.locatieInputBox.TabIndex = 23;
            // 
            // concertzalenInputBox
            // 
            this.concertzalenInputBox.Location = new System.Drawing.Point(79, 114);
            this.concertzalenInputBox.Name = "concertzalenInputBox";
            this.concertzalenInputBox.Size = new System.Drawing.Size(318, 27);
            this.concertzalenInputBox.TabIndex = 24;
            // 
            // ConcertzaalAanmaken
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 556);
            this.Controls.Add(this.concertzalenInputBox);
            this.Controls.Add(this.locatieInputBox);
            this.Controls.Add(this.landDropDown);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ConcertzaalAanmakenButton);
            this.Name = "ConcertzaalAanmaken";
            this.Text = "ConcertzaalAanmaken";
            this.Load += new System.EventHandler(this.ConcertzaalAanmaken_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button ConcertzaalAanmakenButton;
        private Label label2;
        private Label label1;
        private ComboBox landDropDown;
        private Label label3;
        private TextBox locatieInputBox;
        private TextBox concertzalenInputBox;
    }
}