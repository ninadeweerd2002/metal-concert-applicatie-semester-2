﻿namespace ConcertzaalDAL
{
    public class ConcertzaalDTO
    {
        public int ConcertzaalID { get; set; }
        public string ConcertzaalNaam { get; set; }
        public string Locatie { get; set; }
        public string Land { get; set; }

        public ConcertzaalDTO(string concertzaalnaam, string locatie, string land)
        {
            ConcertzaalNaam = concertzaalnaam;
            Locatie = locatie;
            Land = land;
        }

    }
}