﻿namespace BandDAL
{
    public class BandDTO
    {
        public int BandID { get; set; }
        public string BandNaam { get; set; }

        public BandDTO(int bandid, string bandnaam)
        {
            BandID = bandid;
            BandNaam = bandnaam;
        }
    }
}