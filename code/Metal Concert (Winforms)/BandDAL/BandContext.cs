﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace BandDAL
{
    public class BandContext : IBandContext
    {
        private string conn = "Data Source=LAPTOP-VK0TD50R;Initial Catalog=Fontys metal;Integrated Security=True";
        private string query;
        public void AddBand(BandDTO band)
        {
            query = "INSERT INTO bands(bandnaam) VALUES(@bandnaam)";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@bandnaam", band.BandNaam);
                        q.ExecuteNonQuery();
                    }
                }
            } 
            catch (Exception e)
            {
                Console.Write(e);
            }
      
        }
        public void DeleteBand(BandDTO band)
        {
            query = "DELETE FROM bands WHERE idband=@idband";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@idband", band.BandID);
                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
    }
}
