﻿using System.Data.SqlClient;

namespace ConcertDAL
{
    public class ConcertContext : IConcertContext
    {
        private string conn = "Data Source=LAPTOP-VK0TD50R;Initial Catalog=Fontys metal;Integrated Security=True";
        private string query;
        private List<ConcertDTO> concerten = new List<ConcertDTO>();
        public void AddConcert(ConcertDTO concert)
        { 

            query = "INSERT INTO concerten(idband, idconcertzaal, datum, tijdbegin, tijdeinde) VALUES(@idband, @idconcertzaal, @datum, @tijdbegin, @tijdeinde)";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@idband", concert.BandID);
                        q.Parameters.AddWithValue("@idconcertzaal", concert.ConcertzaalID);
                        q.Parameters.AddWithValue("@datum", concert.Datum);
                        q.Parameters.AddWithValue("@tijdbegin", concert.BeginTijd);
                        q.Parameters.AddWithValue("@tijdeinde", concert.EindTijd);
                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
        public void DeleteConcert(ConcertDTO concert)
        {
            query = "DELETE FROM concerten WHERE id=@id";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.Parameters.AddWithValue("@id", concert.ID);
                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
        public List<ConcertDTO> GetAll()
        {
            List<ConcertDTO> dtoC = new List<ConcertDTO>();


            /*query = "SELECT concerten.id, bands.bandnaam, concertzaal.concertzaalnaam, concerten.datum, concerten.tijdbegin, concerten.tijdeinde FROM concerten JOIN bands ON bands.idband=concerten.idband JOIN concertzaal ON concertzaal.idconcertzaal=concerten.idconcertzaal";*/
            query = "SELECT * FROM concerten INNER JOIN bands ON bands.idband=concerten.idband INNER JOIN concertzaal ON concertzaal.idconcertzaal=concerten.idconcertzaal";
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    {
                        q.ExecuteNonQuery();
                        SqlDataReader r = q.ExecuteReader();

                        while (r.Read())
                        {
                            ConcertDTO dto = new ConcertDTO();
                            dto.ID = Convert.ToInt32(r["id"]);
                            dto.BandID = Convert.ToInt32(r["idband"]);
                            dto.ConcertzaalID = Convert.ToInt32(r["idconcertzaal"]);
                            dto.Datum = r["datum"].ToString();
                            dto.BeginTijd = r["tijdbegin"].ToString();
                            dto.EindTijd = r["tijdeinde"].ToString();

                            dtoC.Add(dto);
                        }
                        r.Close();
                        connection.Close();
                        
                    } 
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            return dtoC;
            Console.WriteLine(dtoC);
        }
    }
}