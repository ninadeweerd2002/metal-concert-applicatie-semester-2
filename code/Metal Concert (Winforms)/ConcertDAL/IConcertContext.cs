﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcertDAL
{
    public interface IConcertContext
    {
        public void AddConcert(ConcertDTO concert);
        public void DeleteConcert(ConcertDTO concert);
        public List<ConcertDTO> GetAll();

    }
}
