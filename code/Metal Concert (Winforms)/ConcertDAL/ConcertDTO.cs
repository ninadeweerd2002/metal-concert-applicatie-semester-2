﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcertDAL
{
    public class ConcertDTO
    {
        public int ID { get; set; }
        public int BandID { get; set; }
        public int ConcertzaalID { get; set; }
        public string Datum { get; set; }
        public string BeginTijd { get; set; }
        public string EindTijd { get; set; }

        public ConcertDTO(int id, int bandId, int concertzaalId, string datum, string bTijd, string eTijd)
        {
            ID = id;
            BandID = bandId;
            ConcertzaalID = concertzaalId;
            Datum = datum;
            BeginTijd = bTijd;
            EindTijd = eTijd;
        }

        public ConcertDTO()
        {
        }
    }
}
