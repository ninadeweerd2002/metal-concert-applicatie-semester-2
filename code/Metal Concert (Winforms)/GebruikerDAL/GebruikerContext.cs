﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace GebruikerDAL
{
    public class GebruikerContext : IGebruikerContext
    {
        private string conn = "Data Source=LAPTOP-VK0TD50R;Initial Catalog=Fontys metal;Integrated Security=True";
        private string query;
        public void AddGebruiker(GebruikerDTO gebruiker)
        {
            query = "INSERT INTO gebruikers (username, email, password) VALUES (@username, @email, @password)";

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    connection.Open();
                    using (SqlCommand q = new SqlCommand(query, connection))
                    { 
                        q.Parameters.AddWithValue("@username", gebruiker.Gebruikersnaam);
                        q.Parameters.AddWithValue("@email", gebruiker.Email);
                        q.Parameters.AddWithValue("@password", gebruiker.Password);

                        q.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
    }
}
