﻿namespace GebruikerDAL
{
    public class GebruikerDTO
    {
        public string Gebruikersnaam { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public enum Rol
        {
            Bezoeker,
            Administrator
        }

        public GebruikerDTO(string gb, string email, string pw)
        {
            Gebruikersnaam = gb;
            Email = email;
            Password = pw;

        }
    }
}